﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace Project {
	public class Global : HttpApplication {
		public static DatabaseService.ServiceClient DatabaseServiceClient { get; private set; }

		void Application_Start(object sender, EventArgs e) {
			// Code that runs on application startup
			DatabaseServiceClient = new DatabaseService.ServiceClient("BasicHttpBinding_IService");
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
		}
	}
}