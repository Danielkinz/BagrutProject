﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Project._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

	<div class="jumbotron">
		<h1>Shidea  </h1>
		<p class="lead">The place for sharing ideas and helping others to develop theirs</p>
	</div>

	<div class="row w-100">
		<div class="col-sm-4">
			<h4>Share your ideas </h4>
			<p> Shidea lets you share your ideas in many different ways - by words, pictures, audio and any kind of file </p>
		</div>

		<div class="col-sm-4">
			<h4> Express your opinions </h4>
			<p> Shidea lets you see other people's ideas and express your opinion about them </p>
		</div>

		<div class="col-sm-4">
			<h4> Help others develop </h4>
			<p> By checking other people's ideas you can give them professional help to make an idea come true </p>
		</div>
	</div>

	<br />
	<asp:Label ID="RandomFooterLabel" runat="server"></asp:Label>

</asp:Content>
