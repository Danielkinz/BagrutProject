﻿<%@ Page Title="Ideas" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Section.aspx.cs" Inherits="Project.Idea.Section" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

	<%-- Checks parameters --%>
	<% if (Request["Name"] == null) { Response.Redirect("~/Archives"); return; } %>
	<% if (Request["Page"] != null) PageNum = uint.Parse(Request["Page"]); %>

	<div id="MainArea" runat="server" class="col-sm-9">
		<div class="card">
			<div class="card-header">
				<div class="row">
					<div class="col-3"></div>
					<div class="col-6" style="text-align: center; font-weight: bold; font-size: 1.5rem;"><%= Request["Name"] %></div>
					<div class="col-3">
						<asp:Button ID="NewIdeaButton" runat="server" CssClass="btn btn-success btn-block" Style="margin-left: auto; margin-right: auto;" Text="New Idea" OnClick="NewIdeaButton_Click"></asp:Button>
					</div>
				</div>
			</div>
			<ul class="list-group list-group-flush">
				<% foreach (Project.DatabaseService.Idea idea in Project.Global.DatabaseServiceClient.GetIdeasBySectionPage(Request["Name"], 10, PageNum)) { %>
				<li class="list-group-item"><a href="\Idea\Idea?ID=<%= idea.ID %>"><%= idea.Name %></a></li>
				<% } %>
			</ul>
			<div class="card-footer" style="text-align: center">
				<%-- Creates the paginator --%>
				<ul class="pagination pagionation-lg">
					<%	uint min = PageNum < 3 ? 1 : PageNum - 2;
						uint max = PageNum < 3 ? 1 : PageNum + 2;
						for (uint i = min; i <= max; i++) { %>
					<li class="page-item"><a class="page-link" href="/Section?Name=<%= Request["Name"] %>&Page=<%= i %>"><%= i %> </a></li>
					<% } %>
				</ul>
			</div>
		</div>
	</div>

	<div id="SubArea" class="col-sm-3">
		<div id="NewIdeasPanel" class="card">
			<div class="card-header" style="text-align: center; font-weight: bold">Newest Ideas</div>
			<ul class="list-group list-group-flush">
				<% foreach (Project.DatabaseService.Idea idea in Project.Global.DatabaseServiceClient.GetLatestIdeas()) { %>
				<li class="list-group-item"><a href="\Idea\Idea?ID=<%= idea.ID %>"><%= idea.Name %></a></li>
				<% } %>
			</ul>
		</div>

		<div id="NewMembersPanel" class="card">
			<div class="card-header" style="text-align: center; font-weight: bold">Newest Members</div>
			<ul class="list-group list-group-flush">
				<% foreach (Project.DatabaseService.User user in Project.Global.DatabaseServiceClient.GetNewsetUsers()) { %>
				<li class="list-group-item"><a href="\User\Profile?Name=<%= user.Username %>"><%= user.Username %></a></li>
				<% } %>
			</ul>
		</div>
	</div>

</asp:Content>
