﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Project.User {
	public partial class Login : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {

		}

		protected void LoginButton_Click(object sender, EventArgs e) {
			DatabaseService.User user = Global.DatabaseServiceClient.CheckCredentials(Credentials.Text, Password.Text);

			if (user == null) {
				ErrorAlert.Style["display"] = "block";
				ErrorAlert.InnerHtml = "Username and password mismatch";
			} else {
				Session["User"] = user;
				Session["Username"] = user.Username;
				Response.Redirect("~/");
			}
		}
	}
}