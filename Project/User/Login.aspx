﻿<%@ Page Title="Login" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Project.User.Login" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
	<div class="row w-100">
		<div class="col-sm-3"></div>

		<div class="card col-sm-6 mt-5">
			<div class="card-body">
				<h2 style="text-align: center" class="mt-3">Login </h2>

				<br />
				<div id="ErrorAlert" runat="server" class="alert alert-danger" style="text-align: center; font-size: 1.25rem; display: none;"></div>

				<div class="row">
					<div class="col-sm-5 my-auto" style="text-align: right; white-space: nowrap; overflow: hidden;">
						<label class="control-label font-weight-bold" for="Credentials">Username/Email:</label>
					</div>
					<asp:TextBox ID="Credentials" CssClass="form-control col-sm-6" runat="server" placeholder="Username/Email" />
				</div>

				<br />

				<div class="row">
					<div class="col-sm-5 my-auto" style="text-align: right; white-space: nowrap; overflow: hidden;">
						<label class="control-label font-weight-bold" for="Password">Password:</label>
					</div>
					<asp:TextBox ID="Password" CssClass="form-control col-sm-6" TextMode="Password" runat="server" placeholder="Password" />
				</div>

				<br />

				<div class="row">
					<asp:Button ID="LoginButton" runat="server" CssClass="btn btn-success btn-block" Style="margin-left: auto; margin-right: auto;" OnClientClick="return validate()" OnClick="LoginButton_Click" Text="Login"></asp:Button>
				</div>
			</div>

			<div class="card-footer" style="background-color: white;">
				<p>Don't have an account? <a runat="server" href="SignUp.aspx">Sign up!</a></p>
			</div>
		</div>
	</div>

	<div class="col-sm-3"></div>

	<script type="text/javascript">
		function validate() {
			var CredentialsField = document.getElementById("<%= Credentials.ClientID %>");
			var PasswordField = document.getElementById("<%= Password.ClientID %>");
			var ErrorAlertDiv = document.getElementById("<%= ErrorAlert.ClientID %>");

			if (CredentialsField.value.length < 4) {
				ErrorAlertDiv.style.display = "block";
				ErrorAlertDiv.innerHTML = "The credentials are too short";
				return false;
			}

			if (CredentialsField.value.length > 36) {
				ErrorAlertDiv.style.display = "block";
				ErrorAlertDiv.innerHTML = "The credentials are too long";
				return false;
			}

			if (PasswordField.value.length < 4) {
				ErrorAlertDiv.style.display = "block";
				ErrorAlertDiv.innerHTML = "The password is too short";
				return false;
			}

			if (PasswordField.value.length > 36) {
				ErrorAlertDiv.style.display = "block";
				ErrorAlertDiv.innerHTML = "The password is too long";
				return false;
			}

			return true;
		}
	</script>

</asp:Content>
