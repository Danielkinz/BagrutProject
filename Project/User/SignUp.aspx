﻿<%@ Page Title="Sign Up" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SignUp.aspx.cs" Inherits="Project.User.SignUp" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
	<div class="row w-100">
		<div class="col-sm-3"></div>

		<div class="card col-sm-6 mt-5">
			<div class="card-body">
				<h2 style="text-align: center" class="mt-3">Sign Up</h2>

				<br />
				<div id="ErrorAlert" runat="server" class="alert alert-danger" style="text-align: center; font-size: 1.25rem; display: none;"></div>

				<div class="row">
					<div class="col-sm-5 my-auto" style="text-align: right; white-space: nowrap; overflow: hidden;">
						<label class="control-label font-weight-bold" for="Username">Username:</label>
					</div>
					<asp:TextBox ID="Username" CssClass="form-control col-sm-6" runat="server" placeholder="Username" />
				</div>

				<br />

				<div class="row">
					<div class="col-sm-5 my-auto" style="text-align: right; white-space: nowrap; overflow: hidden;">
						<label class="control-label font-weight-bold" for="Password">Password:</label>
					</div>
					<asp:TextBox ID="Password" CssClass="form-control col-sm-6" TextMode="Password" runat="server" placeholder="Password" />
				</div>

				<br />

				<div class="row">
					<div class="col-sm-5 my-auto" style="text-align: right; white-space: nowrap; overflow: hidden;">
						<label class="control-label font-weight-bold" for="CPassword">Confirm Password: </label>
					</div>
					<asp:TextBox ID="CPassword" CssClass="form-control col-sm-6" TextMode="Password" runat="server" placeholder="Confirm Password" />
				</div>

				<br />

				<div class="row">
					<div class="col-sm-5 my-auto" style="text-align: right; white-space: nowrap; overflow: hidden;">
						<label class="control-label font-weight-bold" for="CPassword">Email:</label>
					</div>
					<asp:TextBox ID="Email" CssClass="form-control col-sm-6" runat="server" placeholder="Email" />
				</div>

				<br />

				<div class="row">
					<asp:Button ID="SignUpButton" runat="server" CssClass="btn btn-success btn-block" Style="margin-left: auto; margin-right: auto;" Text="Sign Up" OnClientClick="return validate()" OnClick="SignUpButton_Click"></asp:Button>
				</div>
			</div>

			<div class="card-footer" style="background-color: white;">
				<p>Already have an account? <a runat="server" href="Login.aspx">Login!</a></p>
			</div>
		</div>
	</div>

	<div class="col-sm-3"></div>

	<script type="text/javascript">
		function validate() {
			var UsernameField = document.getElementById("<%= Username.ClientID%>");
			var PasswordField = document.getElementById("<%= Password.ClientID%>");
			var CPasswordField = document.getElementById("<%= CPassword.ClientID%>");
			var EmailField = document.getElementById("<%= Email.ClientID%>");
			var ErrorAlertDiv = document.getElementById("<%= ErrorAlert.ClientID%>");
			var EmailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;

			if (UsernameField.value.length < 4) {
				ErrorAlertDiv.style.display = "block";
				ErrorAlertDiv.innerHTML = "Username minimum length is 4 characters";
				return false;
			}

			if (UsernameField.value.length > 36) {
				ErrorAlertDiv.style.display = "block";
				ErrorAlertDiv.innerHTML = "Username maximum length is 36 characters";
				return false;
			}

			if (PasswordField.value.length < 4) {
				ErrorAlertDiv.style.display = "block";
				ErrorAlertDiv.innerHTML = "Password minimum length is 4 characters";
				return false;
			}

			if (PasswordField.value.length > 36) {
				ErrorAlertDiv.style.display = "block";
				ErrorAlertDiv.innerHTML = "Password maximum length is 36 characters";
				return false;
			}

			if (CPasswordField.value.length < 4) {
				ErrorAlertDiv.style.display = "block";
				ErrorAlertDiv.innerHTML = "Password maximum length is 4 characters";
				return false;
			}

			if (CPasswordField.value.length > 36) {
				ErrorAlertDiv.style.display = "block";
				ErrorAlertDiv.innerHTML = "Password maximum length is 36 characters";
				return false;
			}

			if (PasswordField.value != CPasswordField.value) {
				ErrorAlertDiv.style.display = "block";
				ErrorAlertDiv.innerHTML = "Passwords mismatch";
				return false;
			}
			
			if (EmailField.value.length < 4) {
				ErrorAlertDiv.style.display = "block";
				ErrorAlertDiv.innerHTML = "Email minimum length is 4 characters";
				return false;
			}

			if (EmailField.value.length > 128) {
				ErrorAlertDiv.style.display = "block";
				ErrorAlertDiv.innerHTML = "Email maximum length is 128 characters";
				return false;
			}

			var RegexResult = EmailField.value.match(EmailRegex);
			if (RegexResult == null || str != RegexResult[0]) {
				ErrorAlertDiv.style.display = "block";
				ErrorAlertDiv.innerHTML = "Invalid email";
				return false;
			}

			return true;
		}
	</script>

</asp:Content>
