﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Project.User {
	public partial class EditProfile : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {
			// Checks that the user is editing his own profile or is an admin
			if (Session["User"] == null || Request["Name"] == null || 
				((string) Session["Username"] != Request["Name"] && ((DatabaseService.User) Session["User"]).Role != "Administrator") ) {
				if (Request["Name"] != null) Response.Redirect("~/User/Profile?ID=" + Request["Name"]);
				else Response.Redirect("~/Archives" + Request["Name"]);
				return;
			}

			if (!Page.IsPostBack) {
				// Gets the user
				DatabaseService.User user = Global.DatabaseServiceClient.GetUser(Request["Name"]);
				if (user == null) {
					Response.Redirect("~/");
				}

				EmailTextBox.Text = user.Email;
				Description.Text = user.Description;

				Role.Items.Add(new ListItem(user.Role, user.Role, true));
				foreach (string s in Project.Global.DatabaseServiceClient.GetRoles()) {
					if (s != user.Role) Role.Items.Add(new ListItem(s, s, true));
				}
				Role.SelectedIndex = 0;
			}
		}

		protected void ChangeDetailsButton_Click(object sender, EventArgs e) {
			if (EmailTextBox.Text.Length == 0 || Description.Text.Length == 0) return;
			Global.DatabaseServiceClient.UpdateUserDetails(Request["Name"], EmailTextBox.Text, Description.Text);
		}

		protected void UpdatePasswordButton_Click(object sender, EventArgs e) {
			if (Password.Text.Length == 0 || CPassword.Text.Length == 0 || Password.Text != CPassword.Text) return;
			Global.DatabaseServiceClient.UpdateUserPassword(Request["Name"], Password.Text);
		}

		protected void UploadPictureButton_Click(object sender, EventArgs e) {
			DatabaseService.Media media = Project.Code.MediaTransfer.UploadMedia(FileUploader.FileName, FileUploader.FileBytes);
			Global.DatabaseServiceClient.SetProfilePicture(Request["Name"], media.ID);
		}

		protected void UpdateManagementButton_Click(object sender, EventArgs e) {
			Global.DatabaseServiceClient.UpdateUserManagement(Request["Name"], Role.SelectedValue);
		}
	}
}