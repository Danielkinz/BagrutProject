﻿<%@ Page Title="Profile Editor" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditProfile.aspx.cs" Inherits="Project.User.EditProfile" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

	<div class="card">
		<div class="card-header">
			<h2 class="m-auto p-auto">Editing user <%= Request["Name"] %></h2>
		</div>
		<ul class="list-group list-group-flush">
			<li class="list-group-item">
				<div class="row">

					<div class="col-4" style="text-align: center;">
						<h3>Details</h3>
						<br />
						<div class="form-group">
							<label class="control-label font-weight-bold" for="EmailTextBox">Email:</label>
							<br />
							<asp:TextBox ID="EmailTextBox" CssClass="form-control m-auto p-auto" runat="server" placeholder="Email" />
						</div>
						<div class="form-group">
							<label class="control-label font-weight-bold" for="Description">Description:</label>
							<br />
							<asp:TextBox ID="Description" TextMode="MultiLine" Style="max-width: 83%;" CssClass="form-control m-auto p-auto" runat="server" placeholder="Description" />
						</div>
						<div class="form-group">
							<asp:Button ID="ChangeDetailsButton" runat="server" CssClass="btn btn-success btn-block" Style="margin-left: auto; margin-right: auto;" Text="Change Details" OnClick="ChangeDetailsButton_Click"></asp:Button>
						</div>
					</div>


					<div class="col-4" style="text-align: center;">
						<h3>Password</h3>
						<br />
						<div class="form-group">
							<label class="control-label font-weight-bold" for="Password">Password:</label>
							<br />
							<asp:TextBox TextMode="Password" ID="Password" CssClass="form-control m-auto p-auto" runat="server" placeholder="Password" />
						</div>
						<div class="form-group">
							<label class="control-label font-weight-bold" for="CPassword">Confirm Password:</label>
							<br />
							<asp:TextBox TextMode="Password" ID="CPassword" CssClass="form-control m-auto p-auto" runat="server" placeholder="Confirm Password" />
						</div>
						<div class="form-group">
							<asp:Button ID="UpdatePasswordButton" runat="server" CssClass="btn btn-success btn-block" Style="margin-left: auto; margin-right: auto;" Text="Change Details" OnClick="UpdatePasswordButton_Click"></asp:Button>
						</div>
					</div>


					<div class="col-4" style="text-align: center;">
						<h3>Profile Picture </h3>
						<br />
						<div class="form-group">
							<label class="control-label font-weight-bold" for="FileUploader">Select file:</label>
							<br />
							<asp:FileUpload ID="FileUploader" CssClass="form-control m-auto p-auto" runat="server" />
						</div>
						<div class="form-group">
							<asp:Button ID="UploadPictureButton" runat="server" CssClass="btn btn-success btn-block" Style="margin-left: auto; margin-right: auto;" Text="Upload Picture" OnClick="UploadPictureButton_Click"></asp:Button>
						</div>
					</div>
				</div>
			</li>
			<%-- Admin options --%>
			<% if (Session["User"] != null && ((Project.DatabaseService.User) Session["User"]).Role == "Administrator") { %>
			<li class="list-group-item">
				<div class="row">
					<div class="col-4" style="text-align: center;">
						<br />
						<h3>Management</h3>
						<br />
						<div class="form-group">
							<label class="control-label font-weight-bold" for="Role">Role:</label>
							<br />
							<asp:DropDownList ID="Role" runat="server" class="form-control m-auto p-auto" />
						</div>
						<div class="form-group">
							<asp:Button ID="UpdateManagementButton" runat="server" CssClass="btn btn-success btn-block" Style="margin-left: auto; margin-right: auto;" Text="Change Details" OnClick="UpdateManagementButton_Click"></asp:Button>
						</div>
					</div>
				</div>
			</li>
			<% } %>
		</ul>
	</div>

</asp:Content>
