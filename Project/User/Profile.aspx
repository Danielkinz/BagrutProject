﻿<%@ Page Title="Profile" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="Project.User.Profile" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

	<% if (Request["Name"] == null && Session["Username"] == null) {
			Response.Redirect("~/?test=true");
			return;
		} else if (Request["Name"] == null && Session["Username"] != null) {
			Response.Redirect("~/User/Profile?Name=" + Session["Username"]);
			return;
		} %>

	<div class="card w-100 mt-5">
		<div class="row">
			<div class="col-sm-1"></div>
			<div class="col-sm-7">
				<br />
				<h1 id="NameField" runat="server">404 Not Found</h1>
				<h4 id="RoleField" runat="server"></h4>
				<br />
				<p id="DescriptionField" runat="server"></p>
				<br />
				<p id="JoinedAtField" runat="server"></p>
			</div>
			<% if (Session["User"] != null && (((Project.DatabaseService.User) Session["User"]).Role == "Administrator" || (string) Session["Username"] == Request["Name"])) { %>
			<div class="col-sm-1">
				<br />
				<a href="EditProfile?Name=<%= Request["Name"]%>" class="btn btn-success">Edit</a>
			</div>
			<% } else { %>
			<div class="col-sm-1""></div>
			<% } %>
			<div class="col-sm-3">
				<asp:Image ID="ProfilePicture" runat="server" />
			</div>
		</div>
	</div>

</asp:Content>
