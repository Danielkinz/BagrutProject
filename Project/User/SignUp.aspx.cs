﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Project.User {
	public partial class SignUp : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {

		}

		protected void SignUpButton_Click(object sender, EventArgs e) {
			if (Global.DatabaseServiceClient.IsUsernameTaken(Username.Text)) {
				ErrorAlert.Style["display"] = "block";
				ErrorAlert.InnerHtml = "This username is already taken";
				return;
			}

			if (Global.DatabaseServiceClient.IsEmailTaken(Email.Text)) {
				ErrorAlert.Style["display"] = "block";
				ErrorAlert.InnerHtml = "This email is already taken";
				return;
			}

			DatabaseService.User user = Global.DatabaseServiceClient.AddUser(Username.Text, Password.Text, Email.Text);
			Session["User"] = user;
			Session["Username"] = user.Username;
			Response.Redirect("~/Default.aspx");
		}
	}
}