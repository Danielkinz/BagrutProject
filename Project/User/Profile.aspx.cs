﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Project.User {
	public partial class Profile : System.Web.UI.Page {
		DatabaseService.User user;

		protected void Page_Load(object sender, EventArgs e) {
			if (Request["Name"] == null && Session["Username"] == null) {
				Response.Redirect("~/?test=true");
				return;
			} else if (Request["Name"] == null && Session["Username"] != null) {
				Response.Redirect("~/User/Profile?Name=" + Session["Username"]);
				return;
			}

			user = Global.DatabaseServiceClient.GetUser(Request["Name"]);
			if (user == null) {
				Response.Redirect("~/");
				return;
			}

			NameField.InnerHtml = user.Username;
			DescriptionField.InnerHtml = user.Description;
			RoleField.InnerHtml = user.Role;
			RoleField.Attributes["style"] = Global.DatabaseServiceClient.GetRoleStyle(user.Role);
			JoinedAtField.InnerHtml = "Joined at: " + user.JoinTime.ToString();

			DatabaseService.Media media = Global.DatabaseServiceClient.GetProfilePicture(Request["Name"]);
			if (media.ID == 1) {
				ProfilePicture.ImageUrl = "/Images/Default_Profile_Picture.png";
			} else {
				ProfilePicture.ImageUrl = "data:image;base64," + Convert.ToBase64String(Project.Code.MediaTransfer.DownloadMedia(media.ID));
			}
			ProfilePicture.Width = 256;
			ProfilePicture.Height = 256;
		}
	}
}