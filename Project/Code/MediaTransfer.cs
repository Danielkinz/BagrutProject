﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project.Code {
	public class MediaTransfer {
		public static DatabaseService.Media UploadMedia(string filename, byte[] raw) {
			int uploadSession = Global.DatabaseServiceClient.StartMediaUpload(filename, (uint) raw.Length);
			int uploaded = 0;
			byte[] buffer = new byte[8192];

			// Uploads chunks of 8kb
			while (uploaded < raw.Length - 8192) {
				Array.Copy(raw, uploaded, buffer, 0, 8192);
				Global.DatabaseServiceClient.StreamMedia(uploadSession, buffer);
				uploaded += 8192;
			}

			buffer = new byte[raw.Length - uploaded];
			Array.Copy(raw, uploaded, buffer, 0, raw.Length - uploaded);
			Global.DatabaseServiceClient.StreamMedia(uploadSession, buffer);
			return Global.DatabaseServiceClient.EndMediaUpload(uploadSession);
		}

		public static byte[] DownloadMedia(uint mediaID) {
			DatabaseService.Media media = Global.DatabaseServiceClient.GetMedia(mediaID);
			byte[] raw = new byte[media.FileSize];
			int downloaded = 0;
			int downloadSession = Global.DatabaseServiceClient.StartMediaDownload(mediaID);
			byte[] buffer;

			while (downloaded < media.FileSize) {
				buffer = Global.DatabaseServiceClient.DownloadMedia(downloadSession);
				Array.Copy(buffer, 0, raw, downloaded, buffer.Length);
				downloaded += buffer.Length;
			}

			return raw;
		}
	}
}