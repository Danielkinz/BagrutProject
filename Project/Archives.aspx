﻿<%@ Page Title="Archives" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Archives.aspx.cs" Inherits="Project.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

	<div id="MainArea" class="col-sm-9">

		<%-- Generates the sections --%>
		<% foreach (string type in Project.Global.DatabaseServiceClient.GetSectionTypes()) { %>
		<div class="card">
			<div class="card-header" style="text-align: center; font-weight: bold;"><%= type %></div>
			<ul class="list-group list-group-flush">
				<% foreach (string section in Project.Global.DatabaseServiceClient.GetSections(type)) { %>
				<li class="list-group-item"><a href="\Section?Name=<%= section %>"><%= section %></a></li>
				<% } %>
			</ul>
		</div>
		<% } %>
	</div>

	<div id="SubArea" class="col-sm-3">
		<div id="NewIdeasPanel" class="card">
			<div class="card-header" style="text-align: center; font-weight:bold">Newest Ideas</div>
			<ul class="list-group list-group-flush">
				<% foreach (Project.DatabaseService.Idea idea in Project.Global.DatabaseServiceClient.GetLatestIdeas()) { %>
				<li class="list-group-item"><a href="\Idea\Idea?ID=<%= idea.ID %>"><%= idea.Name %></a></li>
				<% } %>
			</ul>
		</div>

		<div id="NewMembersPanel" class="card">
			<div class="card-header" style="text-align: center; font-weight:bold">Newest Members</div>
			<ul class="list-group list-group-flush">
				<% foreach (Project.DatabaseService.User user in Project.Global.DatabaseServiceClient.GetNewsetUsers()) { %>
				<li class="list-group-item"><a href="\User\Profile?Name=<%= user.Username %>"><%= user.Username %></a></li>
				<% } %>
			</ul>
		</div>
	</div>

</asp:Content>
