﻿<%@ Page Title="Create Idea" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="IdeaCreator.aspx.cs" Inherits="Project.Idea.IdeaCreator" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

	<% if (Session["Username"] == null) { %>

	<div class="alert alert-danger">
		You must be logged in to create a new idea
	</div>

	<% } else { %>

	<div class="card">
		<div class="card-body">
			<h2 style="text-align: center" class="mt-3">New Topic</h2>

			<br />
			<div id="ErrorAlert" runat="server" class="alert alert-danger" style="text-align: center; font-size: 1.25rem; display: none;"></div>

			<div class="row">
				<div class="col-sm-2 my-auto" style="text-align: right; white-space: nowrap; overflow: hidden;">
					<label class="control-label font-weight-bold" for="IdeaName">Idea Name:</label>
				</div>
				<asp:TextBox ID="IdeaName" CssClass="form-control col-sm-9" runat="server" placeholder="Give your idea a fancy name" />
			</div>

			<br />

			<div class="row">
				<div class="col-sm-2 my-auto" style="text-align: right; white-space: nowrap; overflow: hidden;">
					<label class="control-label font-weight-bold" for="IdeaDescription">IdeaDescription:</label>
				</div>
				<asp:TextBox ID="IdeaDescription" runat="server" TextMode="MultiLine" CssClass="form-control col-sm-9" Rows="5" Style="max-width: 100%;" placeholder="Write some more information about your idea here"></asp:TextBox>
			</div>

			<br />

			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-3">
					<div class="row">
						<asp:FileUpload ID="MediaUpload" CssClass="form-control m-auto" AllowMultiple="true" runat="server" onchange="handleFiles(this.files);" />
					</div>
				</div>
				<div class="col-sm-3"></div>
				<div class="col-sm-3">
					<div class="row">
						<asp:Button ID="SubmitButton" runat="server" CssClass="btn btn-success btn-block mt-1" Style="margin-left: auto; margin-right: auto;" Text="Submit" OnClientClick="return validate()" OnClick="SubmitButton_Click"></asp:Button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		function validate() {
			var NameField = document.getElementById("<%= IdeaName.ClientID %>");
			var DescriptionField = document.getElementById("<%= IdeaDescription.ClientID %>");
			var ErrorAlertField = document.getElementById("<%= ErrorAlert.ClientID%>");

			if (NameField.value.length < 4) {
				ErrorAlertField.innerHTML = "The name is too short";
				ErrorAlertField.style.display = "block";
				return false;
			}

			if (NameField.value.length > 128) {
				ErrorAlertField.innerHTML = "The name is too long";
				ErrorAlertField.style.display = "block";
				return false;
			}

			if (DescriptionField.value.length < 4) {
				ErrorAlertField.innerHTML = "The description is too short";
				ErrorAlertField.style.display = "block";
				return false;
			}

			if (DescriptionField.value.length > 2048) {
				ErrorAlertField.innerHTML = "Description maximum characters reached";
				ErrorAlertField.style.display = "block";
				return false;
			}

			return true;
		}
	</script>

	<% } %>
</asp:Content>
