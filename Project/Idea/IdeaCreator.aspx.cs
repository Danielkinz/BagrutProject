﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Project.Idea {
	public partial class IdeaCreator : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {

		}

		protected void SubmitButton_Click(object sender, EventArgs e) {
			DatabaseService.Idea idea = Global.DatabaseServiceClient.AddIdea(IdeaName.Text, IdeaDescription.Text, (string) Session["Username"], Request["Section"]);

			if (MediaUpload.HasFiles) {
				foreach (var file in MediaUpload.PostedFiles) {
					var binaryReader = new BinaryReader(file.InputStream);
					DatabaseService.Media media = Project.Code.MediaTransfer.UploadMedia(file.FileName, binaryReader.ReadBytes(file.ContentLength));
					binaryReader.Close();
					Global.DatabaseServiceClient.LinkIdeaMedia(idea.ID, media.ID);
				}
			}
			Response.Redirect("Idea?ID=" + idea.ID);
		}
	}
}