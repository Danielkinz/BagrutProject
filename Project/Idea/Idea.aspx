﻿<%@  Title="Idea Editor" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Idea.aspx.cs" Inherits="Project.Idea.Idea" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

	<style>
		.NameLink:link {
			color: black;
			text-decoration: none;
		}

		.NameLink:hover {
			color: gray;
			text-decoration: none;
		}
	</style>

	<div class="card">
		<ul class="list-group list-group-flush">
			<li class="list-group-item">
				<br />
				<div class="row">
					<div class="col-sm-3" style="text-align: center">
						<a id="NameLink" href="#" class="NameLink" runat="server">
							<h2 id="PosterName" runat="server"></h2>
						</a>
						<asp:Image ID="ProfilePicture" runat="server" Style="width: 128px; height: 128px" CssClass="my-2" />
						<h5 id="PosterRole" runat="server"></h5>
					</div>

					<div class="col-sm-7">
						<h2 id="IdeaName" runat="server">Not Found! </h2>
						<p id="IdeaContent" style="font-size: 1.25rem" runat="server"></p>
						<p id="PostedAt" style="color: slategray; font-size: 0.85rem;" runat="server"></p>
					</div>

					<% if (Session["User"] != null && ((Project.DatabaseService.User) Session["User"]).Role == "Administrator") {%>
					<div class="col-sm-2">
						<a href="IdeaEditor?ID= <%= Request["ID"] %>" class="btn btn-success">Edit </a>
					</div>
					<% } %>
				</div>
			</li>

			<% if (Project.Global.DatabaseServiceClient.GetIdeaMediaList(uint.Parse(Request["ID"])).Length > 0) { %>

			<li class="list-group-item p-y-2">
				<div class="row">
					<div class="col-sm-2" style="text-align: right; vertical-align: central; font-weight: bold;">
						Attached files:
					</div>
					<div class="col-sm-8">
						<% foreach (var file in Project.Global.DatabaseServiceClient.GetIdeaMediaList(uint.Parse(Request["ID"]))) { %>
						<a href="/DownloadMedia?Id=<%= file.ID %>" class="alert alert-success mx-1">
							<strong><%= file.Name %></strong>
						</a>
						<% } %>
					</div>
				</div>
			</li>

			<% } %>
		</ul>
	</div>

	<% foreach (Project.DatabaseService.Comment comment in Project.Global.DatabaseServiceClient.GetComments(uint.Parse(Request["id"].ToString()))) { %>
	<div class="card">
		<ul class="list-group list-group-flush">
			<li class="list-group-item">
				<br />
				<div class="row">
					<div class="col-sm-3" style="text-align: center">
						<a href="/User/Profile?Name=<%= comment.Poster.Username %>" class="NameLink">
							<h2><%= comment.Poster.Username %> </h2>
						</a>
						<%	Project.DatabaseService.Media media = Project.Global.DatabaseServiceClient.GetProfilePicture(comment.Poster.Username);
							if (media.ID == 1) { %>
						<img class="my-2" src="/Images/Default_Profile_Picture.png" style="height: 128px; width: 128px" />
						<% } else { %>
						<img class="my-2" src="data:image;base64,<%= Convert.ToBase64String(Project.Code.MediaTransfer.DownloadMedia(media.ID)) %>" style="height: 128px; width: 128px" />
						<% } %>

						<h5 <%= "style=\"" + Project.Global.DatabaseServiceClient.GetRoleStyle(comment.Poster.Role) + "\"" %>><%= comment.Poster.Role %></h5>
					</div>
					<div class="col-sm-7">
						<p style="font-size: 1.25rem"><%= comment.Description %> </p>
						<p style="color: slategray; font-size: 0.85rem;">Posted at <%= comment.PostTime.ToString() %> </p>
					</div>
					<% if (Session["User"] != null && ((Project.DatabaseService.User) Session["User"]).Role == "Administrator") {%>
					<div class="col-sm-2">
						<a href="CommentEditor?ID= <%= comment.CommentID %>&Idea= <%= Request["ID"]%>" class="btn btn-success">Edit </a>
					</div>
					<% } %>
				</div>
			</li>

			<% if (Project.Global.DatabaseServiceClient.GetCommentMediaList(comment.CommentID).Length > 0) { %>

			<li class="list-group-item p-y-2">
				<div class="row">
					<div class="col-sm-2" style="text-align: right; vertical-align: central; font-weight: bold;">
						Attached files:
					</div>
					<div class="col-sm-8">
						<% foreach (var file in Project.Global.DatabaseServiceClient.GetCommentMediaList(comment.CommentID)) { %>
						<a href="/DownloadMedia?Id=<%= file.ID %>" class="alert alert-success mx-1">
							<strong><%= file.Name %></strong>
						</a>
						<% } %>
					</div>
				</div>
			</li>

			<% } %>
		</ul>
	</div>
	<% } %>

	<% if (Session["User"] == null) { %>

	<div class="alert alert-danger">
		You have to sign in to be able to comment
	</div>

	<% } else { %>

	<div class="card">
		<div class="card-header"><strong>Post your comment here: </strong></div>
		<div class="card-body">
			<div class="row">
				<div class="col-sm-3" style="text-align: center">
					<% {
							Project.DatabaseService.User user = (Project.DatabaseService.User) Session["User"]; %>

					<a href="/User/Profile?Name=<%= user.Username %>" class="NameLink">
						<h2><%= user.Username %> </h2>
					</a>
					<%	Project.DatabaseService.Media media = Project.Global.DatabaseServiceClient.GetProfilePicture(user.Username);
						if (media.ID == 1) { %>
					<img class="my-2" src="/Images/Default_Profile_Picture.png" style="height: 128px; width: 128px" />
					<% } else { %>
					<img class="my-2" src="data:image;base64,<%= Convert.ToBase64String(Project.Code.MediaTransfer.DownloadMedia(media.ID)) %>" style="height: 128px; width: 128px" />
					<% } %>

					<h5 <%= "style=\"" + Project.Global.DatabaseServiceClient.GetRoleStyle(user.Role) + "\"" %>><%= user.Role %></h5>

					<% } %>
				</div>
				<div class="col-8">
					<div id="ErrorAlert" runat="server" class="alert alert-danger" style="text-align: center; font-size: 1.25rem; display: none;"></div>
					
					<asp:TextBox ID="CommentTextArea" TextMode="MultiLine" runat="server" CssClass="form-control" Rows="5" Style="max-width: 100%;"></asp:TextBox>
					<br />
					<div class="row">
						<div class="col-5" style="text-align: center;">
							<asp:FileUpload ID="MediaUpload" CssClass="form-control m-auto" AllowMultiple="true" runat="server" onchange="handleFiles(this.files);" />
						</div>
						<div class="col-3"></div>
						<div class="col-4" style="text-align: center;">
							<asp:Button ID="SubmitButton" runat="server" CssClass="btn btn-success btn-block" Style="margin-left: auto; margin-right: auto;" Text="Submit" OnClientClick="return validateComment()" OnClick="SubmitButton_Click"></asp:Button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		function validateComment() {
			var ContentField = document.getElementById("<%= CommentTextArea.ClientID %>");
			var ErrorAlertField = document.getElementById("<%= ErrorAlert.ClientID%>");

			if (ContentField.value.length < 4) {
				ErrorAlertField.innerHTML = "The comment is too short";
				ErrorAlertField.style.display = "block";
				return false;
			}

			if (ContentField.value.length > 2048) {
				ErrorAlertField.innerHTML = "Maximum characters reached";
				ErrorAlertField.style.display = "block";
				return false;
			}

			return true;
		}
	</script>

	<% } %>
</asp:Content>
