﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Project.Idea {
	public partial class IdeaEditor : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {
			if (!Page.IsPostBack) {
				if (Request["ID"] == null) return;

				DatabaseService.Idea idea = Global.DatabaseServiceClient.GetIdea(uint.Parse(Request["ID"]));
				IdeaName.Text = idea.Name;
				IdeaDescription.Text = idea.Description;
			}
		}

		protected void SubmitButton_Click(object sender, EventArgs e) {
			Global.DatabaseServiceClient.UpdateIdea(uint.Parse(Request["ID"]), IdeaName.Text, IdeaDescription.Text);
			Response.Redirect("~/Idea/Idea?ID=" + Request["ID"]);
		}

		protected void DeleteButton_Click(object sender, EventArgs e) {
			Global.DatabaseServiceClient.DeleteIdea(uint.Parse(Request["ID"]));
			Response.Redirect("~/Archives");
		}
	}
}