﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Project.Idea {
	public partial class CommentEditor : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {
			if (!Page.IsPostBack) {
				if (Request["ID"] == null) return;

				DatabaseService.Comment comment = Global.DatabaseServiceClient.GetComment(uint.Parse(Request["ID"]));
				Content.Text = comment.Description;
			}
		}

		protected void DeleteButton_Click(object sender, EventArgs e) {
			Global.DatabaseServiceClient.DeleteComment(uint.Parse(Request["ID"]));
			if (Request["Idea"] == null)
				Response.Redirect("~/Archives");
			else
				Response.Redirect("~/Idea/Idea?ID=" + Request["Idea"]);
		}

		protected void SubmitButton_Click(object sender, EventArgs e) {
			Global.DatabaseServiceClient.UpdateComment(uint.Parse(Request["ID"]), Content.Text);
			if (Request["Idea"] == null)
				Response.Redirect("~/Archives");
			else
				Response.Redirect("~/Idea/Idea?ID=" + Request["Idea"]);
		}
	}
}