﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Project.Idea {
	public partial class Idea : System.Web.UI.Page {

		private DatabaseService.Idea _idea = null;

		public DatabaseService.Idea IdeaInfo { get { return _idea; } }

		protected void Page_Load(object sender, EventArgs e) {
			// If no idea id was specified returns to the main archives page
			if (Request["id"] == null) {
				Response.Redirect("/Archives");
				return;
			}

			// Gets idea info
			uint id = uint.Parse(Request["id"].ToString());
			_idea = Global.DatabaseServiceClient.GetIdea(id);
			if (_idea == null) {
				return;
			}

			// Sets the idea content
			PosterName.InnerHtml = IdeaInfo.Poster.Username;
			PosterRole.InnerHtml = IdeaInfo.Poster.Role;
			PosterRole.Attributes["style"] = Global.DatabaseServiceClient.GetRoleStyle(IdeaInfo.Poster.Role);

			IdeaName.InnerHtml = IdeaInfo.Name;
			IdeaContent.InnerHtml = IdeaInfo.Description;
			PostedAt.InnerHtml = "Posted at " + IdeaInfo.PostTime.ToString();

			DatabaseService.Media media = Global.DatabaseServiceClient.GetProfilePicture(IdeaInfo.Poster.Username);
			if (media.ID == 1) {
				ProfilePicture.ImageUrl = "/Images/Default_Profile_Picture.png";
			} else {
				ProfilePicture.ImageUrl = "data:image;base64," + Convert.ToBase64String(Project.Code.MediaTransfer.DownloadMedia(media.ID));
			}
			ProfilePicture.Width = 128;
			ProfilePicture.Height = 128;

			NameLink.HRef = "/User/Profile?Name=" + IdeaInfo.Poster.Username;
		}

		protected void ClearButton_Click(object sender, EventArgs e) {
			CommentTextArea.Text = "";
		}

		protected void SubmitButton_Click(object sender, EventArgs e) {
			DatabaseService.Comment comment =  Global.DatabaseServiceClient.AddComment(IdeaInfo.ID, CommentTextArea.Text, (string) Session["Username"]);

			if (MediaUpload.HasFiles) {
				foreach (var file in MediaUpload.PostedFiles) {
					BinaryReader binaryReader = new BinaryReader(file.InputStream);
					DatabaseService.Media media = Project.Code.MediaTransfer.UploadMedia(file.FileName, binaryReader.ReadBytes(file.ContentLength));
					binaryReader.Close();
					Global.DatabaseServiceClient.LinkCommentMedia(comment.CommentID, media.ID);
				}
			}

			Response.Redirect("Idea?id=" + IdeaInfo.ID);
		}
	}
}