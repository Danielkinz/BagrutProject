﻿<%@ Page Title="Comment Editor" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CommentEditor.aspx.cs" Inherits="Project.Idea.CommentEditor" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

	<%-- Checks if the user is allowed to edit this idea --%>
	<% if (Session["Username"] == null || 
			Request["ID"] == null ||  
				(Project.Global.DatabaseServiceClient.GetComment(uint.Parse(Request["ID"])).Poster.Username != (string) Session["Username"] 
				&& ((Project.DatabaseService.User)Session["User"]).Role != "Administrator")) { %>

	<div class="alert alert-danger">
		You cannot edit this comment
	</div>

	<% } else { %>

	<div class="card">
		<div class="card-body">
			<h2 class="mt-3 ml-5">Editing comment</h2>

			<br />
			<div id="ErrorAlert" runat="server" class="alert alert-danger" style="text-align: center; font-size: 1.25rem; display: none;"></div>

			<div class="row">
				<div class="col-sm-2 my-auto" style="text-align: right; white-space: nowrap; overflow: hidden;">
					<label class="control-label font-weight-bold" for="Content">Idea Description:</label>
				</div>
				<asp:TextBox ID="Content" runat="server" TextMode="MultiLine" CssClass="form-control col-sm-9" Rows="5" Style="max-width: 100%;" placeholder="New description"></asp:TextBox>
			</div>

			<br />

			<div class="row">
				<div class="col-sm-1"></div>
				<div class="col-sm-3">
					<asp:Button ID="DeleteButton" runat="server" CssClass="btn btn-danger btn-block" Style="margin-left: auto; margin-right: auto;" Text="Delete" OnClick="DeleteButton_Click"></asp:Button>
				</div>
				<div class="col-sm-4"></div>
				<div class="col-sm-3">
					<asp:Button ID="SubmitButton" runat="server" CssClass="btn btn-success btn-block" Style="margin-left: auto; margin-right: auto;" Text="Submit" OnClientClick="return validate()" OnClick="SubmitButton_Click"></asp:Button>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		function validate() {
			var Content = document.getElementById("<%= Content.ClientID %>");
			var ErrorAlertField = document.getElementById("<%= ErrorAlert.ClientID%>");

			if (Content.value.length < 4) {
				ErrorAlertField.innerHTML = "The content is too short";
				ErrorAlertField.style.display = "block";
				return false;
			}

			if (Content.value.length > 2048) {
				ErrorAlertField.innerHTML = "The content is too long";
				ErrorAlertField.style.display = "block";
				return false;
			}

			return true;
		}
	</script>

	<% } %>

</asp:Content>