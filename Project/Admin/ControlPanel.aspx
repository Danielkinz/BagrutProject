﻿<%@ Page Title="Control Panel" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ControlPanel.aspx.cs" Inherits="Project.Admin.ControlPanel" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

	<%-- Admin only page --%>
	<% if ((Session["Username"] == null) || ((Project.DatabaseService.User) Session["User"]).Role != "Administrator") {
			Response.Redirect("~/");
			return;
		} else { %>

	<div class="card">
		<ul class="list-group list-group-flush">
			<li class="list-group-item">
				<h2 style="text-align: center" class="mt-3">Site Control Panel</h2>
			</li>
			<li class="list-group-item">
				<div id="ErrorAlert" runat="server" class="alert alert-danger" style="text-align: center; font-size: 1.25rem; display: none;"></div>
				<br />
				<div class="row">
					<div class="col-4">
						<h3 style="text-align: center">New Section Type</h3>
						<br />
						<div class="form-group">
							<label class="control-label font-weight-bold ml-4" for="SectionTypeTB">Section type name:</label>
							<br />
							<asp:TextBox ID="SectionTypeTB" CssClass="form-control m-auto p-auto" runat="server" placeholder="Section type name" />
						</div>
						<div class="form-group">
							<asp:Button ID="SectionTypeAddButton" runat="server" CssClass="btn btn-success btn-block" Style="margin-left: auto; margin-right: auto;" Text="Add New Section Type" OnClientClick="return ValidateCreateSectionType();" OnClick="SectionTypeAddButton_Click"></asp:Button>
						</div>
					</div>
					<div class="col-4">
						<h3 style="text-align: center">Delete Section Type</h3>
						<br />
						<div class="form-group">
							<label class="control-label font-weight-bold ml-4" for="SectionTypeDeleteList">Select Section Type:</label>
							<br />
							<asp:DropDownList ID="SectionTypeDeleteList" runat="server" class="form-control m-auto p-auto" />
						</div>
						<div class="form-group">
							<asp:Button ID="SectionTypeDeleteButton" runat="server" CssClass="btn btn-danger btn-block" Style="margin-left: auto; margin-right: auto;" Text="Delete Section Type" OnClientClick="return confirm('Are you sure that you want to delete this section type?')" OnClick="SectionTypeDeleteButton_Click"></asp:Button>
						</div>
					</div>
				</div>
				<br />
			</li>
			<li class="list-group-item">
				<br />
				<div class="row">
					<div class="col-4">
						<h3 style="text-align: center">New Section</h3>
						<br />
						<div class="form-group">
							<label class="control-label font-weight-bold ml-4" for="SectionNameTB">Section name:</label>
							<br />
							<asp:TextBox ID="SectionNameTB" CssClass="form-control m-auto p-auto" runat="server" placeholder="Section name" />
						</div>
						<div class="form-group">
							<label class="control-label font-weight-bold ml-4" for="SectionTypeDropDown">Section Type:</label>
							<br />
							<asp:DropDownList ID="SectionTypeDropDown" runat="server" class="form-control m-auto p-auto" />
						</div>
						<div class="form-group">
							<asp:Button ID="SectionAddButton" runat="server" CssClass="btn btn-success btn-block" Style="margin-left: auto; margin-right: auto;" Text="Add New Section" OnClientClick="return ValidateCreateSection()" OnClick="SectionAddButton_Click"></asp:Button>
						</div>
					</div>
					<div class="col-4">
						<h3 style="text-align: center"> Delete Section</h3>
						<br />
						<div class="form-group">
							<label class="control-label font-weight-bold ml-4" for="SectionDeleteList">Select Section:</label>
							<br />
							<asp:DropDownList ID="SectionDeleteList" runat="server" class="form-control m-auto p-auto" />
						</div>
						<div class="form-group">
							<asp:Button ID="SectionDeleteButton" runat="server" CssClass="btn btn-danger btn-block" Style="margin-left: auto; margin-right: auto;" Text="Delete Section" OnClientClick="return confirm('Are you sure that you want to delete this section?')" OnClick="SectionDeleteButton_Click"></asp:Button>
						</div>
					</div>
				</div>
			</li>
			<li class="list-group-item">
				<br />
				<div class="row">
					<div class="col-4">
						<h3 style="text-align: center">New Role</h3>
						<br />
						<div class="form-group">
							<label class="control-label font-weight-bold ml-4" for="RoleNameTB">Role name:</label>
							<br />
							<asp:TextBox ID="RoleNameTB" CssClass="form-control m-auto p-auto" runat="server" placeholder="Role Name" />
						</div>
						<div class="form-group">
							<label class="control-label font-weight-bold ml-4" for="RoleStyleTB">Role Style:</label>
							<br />
							<asp:TextBox ID="RoleStyleTB" CssClass="form-control m-auto p-auto" runat="server" placeholder="Role Style" />
						</div>
						<div class="form-group">
							<asp:Button ID="AddRoleButton" runat="server" CssClass="btn btn-success btn-block" Style="margin-left: auto; margin-right: auto;" Text="Add Role" OnClientClick="return ValidateCreateRole()" OnClick="AddRoleButton_Click"></asp:Button>
						</div>
					</div>

					<div class="col-4">
						<h3 style="text-align: center">Edit Role</h3>
						<br />
						<div class="form-group">
							<label class="control-label font-weight-bold ml-4" for="RoleEditDropDown">Select Role:</label>
							<br />
							<asp:DropDownList ID="RoleEditDropDown" runat="server" class="form-control m-auto p-auto" />
						</div>
						<div class="form-group">
							<label class="control-label font-weight-bold ml-4" for="RoleStyleEditTB">Role Style:</label>
							<br />
							<asp:TextBox ID="RoleStyleEditTB" CssClass="form-control m-auto p-auto" runat="server" placeholder="Role Style" />
						</div>
						<div class="form-group">
							<asp:Button ID="EditRoleButton" runat="server" CssClass="btn btn-success btn-block" Style="margin-left: auto; margin-right: auto;" Text="Edit Role" OnClick="EditRoleButton_Click"></asp:Button>
						</div>
					</div>

					<div class="col-4">
						<h3 style="text-align: center">Delete Role</h3>
						<br />
						<div class="form-group">
							<label class="control-label font-weight-bold ml-4" for="RoleDeleteDropDown">Select Role:</label>
							<br />
							<asp:DropDownList ID="RoleDeleteDropDown" runat="server" class="form-control m-auto p-auto" />
						</div>
						<div class="form-group">
							<asp:Button ID="DeleteRoleButton" runat="server" CssClass="btn btn-danger btn-block" Style="margin-left: auto; margin-right: auto;" Text="Delete Role" OnClientClick="return ValidateDeleteRole()" OnClick="DeleteRoleButton_Click"></asp:Button>
						</div>
					</div>
				</div>
				<br />
			</li>
		</ul>
	</div>

	<script type="text/javascript">
		ErrorAlertField = document.getElementById("<%= ErrorAlert.ClientID%>");

		function ValidateCreateSectionType() {
			var SectionTypeField = document.getElementById("<%= SectionTypeTB.ClientID %>");

			if (SectionTypeField.value.length == 0) {
				ErrorAlertField.innerHTML = "Section type name cannot be empty";
				ErrorAlertField.style.display = "block";
				return false;
			}

			return true;
		}

		function ValidateCreateSection() {
			var SectionNameField = document.getElementById("<%= SectionNameTB.ClientID %>");

			if (SectionNameField.value.length == 0) {
				ErrorAlertField.innerHTML = "Section name cannot be empty";
				ErrorAlertField.style.display = "block";
				return false;
			}

			return true;
		}

		function ValidateCreateRole() {
			var RoleNameField = document.getElementById("<%= RoleNameTB.ClientID %>");

			if (RoleNameField.value.length == 0) {
				ErrorAlertField.innerHTML = "Role name cannot be empty";
				ErrorAlertField.style.display = "block";
				return false;
			}

			return true;
		}

		function ValidateDeleteRole() {
			var DeleteRoleDropDownField = document.getElementById("<%= RoleDeleteDropDown.ClientID %>");

			if (DeleteRoleDropDownField.value == "Member" || DeleteRoleDropDownField.value == "Administrator") {
				ErrorAlertField.innerHTML = "You cannot delete this role";
				ErrorAlertField.style.display = "block";
			}

			return true;
		}

	</script>
	<% } %>
</asp:Content>
