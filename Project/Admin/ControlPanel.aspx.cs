﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Project.Admin {
	public partial class ControlPanel : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {
			if (!Page.IsPostBack) {
				UpdateDropDowns();
			}
		}

		protected void UpdateDropDowns() {
			RoleDeleteDropDown.Items.Clear();
			RoleEditDropDown.Items.Clear();
			SectionTypeDropDown.Items.Clear();
			RoleDeleteDropDown.Items.Clear();
			SectionDeleteList.Items.Clear();

			foreach (string role in Global.DatabaseServiceClient.GetRoles()) {
				RoleDeleteDropDown.Items.Add(new ListItem(role, role, true));
				RoleEditDropDown.Items.Add(new ListItem(role, role, true));
			}

			foreach (string sectionType in Global.DatabaseServiceClient.GetSectionTypes()) {
				SectionTypeDropDown.Items.Add(new ListItem(sectionType, sectionType, true));
				SectionTypeDeleteList.Items.Add(new ListItem(sectionType, sectionType, true));

				foreach (string section in Global.DatabaseServiceClient.GetSections(sectionType)) {
					SectionDeleteList.Items.Add(new ListItem(section, section, true));
				}
			}

			RoleDeleteDropDown.SelectedIndex = 0;
			RoleEditDropDown.SelectedIndex = 0;
			SectionTypeDropDown.SelectedIndex = 0;
			SectionTypeDeleteList.SelectedIndex = 0;
			SectionDeleteList.SelectedIndex = 0;
		}

		protected void SectionTypeAddButton_Click(object sender, EventArgs e) {
			Global.DatabaseServiceClient.CreateSectionType(SectionTypeTB.Text);
			UpdateDropDowns();
		}

		protected void SectionAddButton_Click(object sender, EventArgs e) {
			Global.DatabaseServiceClient.CreateSection(SectionNameTB.Text, SectionTypeDropDown.SelectedValue);
		}

		protected void AddRoleButton_Click(object sender, EventArgs e) {
			Global.DatabaseServiceClient.AddRole(RoleNameTB.Text, RoleStyleTB.Text);
			UpdateDropDowns();
		}

		protected void EditRoleButton_Click(object sender, EventArgs e) {
			Global.DatabaseServiceClient.UpdateRole(RoleEditDropDown.SelectedValue, RoleStyleEditTB.Text);
		}

		protected void DeleteRoleButton_Click(object sender, EventArgs e) {
			string selectedRole = RoleDeleteDropDown.SelectedValue;
			if (selectedRole == "Member" || selectedRole == "Administrator") {
				ErrorAlert.InnerHtml = "You cannot delete this role";
				ErrorAlert.Style["display"] = "block";
				return;
			}
			Global.DatabaseServiceClient.DeleteRole(RoleDeleteDropDown.SelectedValue);
			UpdateDropDowns();
		}

		protected void SectionTypeDeleteButton_Click(object sender, EventArgs e) {
			Global.DatabaseServiceClient.DeleteSectionType(SectionTypeDeleteList.SelectedValue);
			UpdateDropDowns();
		}

		protected void SectionDeleteButton_Click(object sender, EventArgs e) {
			Global.DatabaseServiceClient.DeleteSection(SectionDeleteList.SelectedValue);
			UpdateDropDowns();
		}
	}
}