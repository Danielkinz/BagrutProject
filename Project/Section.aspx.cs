﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Project.Idea {
	public partial class Section : System.Web.UI.Page {
		protected uint PageNum = 1;

		protected void Page_Load(object sender, EventArgs e) {
		
		}

		protected void NewIdeaButton_Click(object sender, EventArgs e) {
			Response.Redirect("/Idea/IdeaCreator?Section=" + Request["Name"]);
		}
	}
}