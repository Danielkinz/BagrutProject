﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Project {
	public partial class DownloadMedia : System.Web.UI.Page {
		protected void Page_Load(object sender, EventArgs e) {
			if (Request["ID"] == null) {
				return;
			}

			DatabaseService.Media media = Global.DatabaseServiceClient.GetMedia(uint.Parse(Request["ID"]));
			byte[] raw = Code.MediaTransfer.DownloadMedia(uint.Parse(Request["ID"]));

			Response.Write("Download Successful");

			Response.Clear();
			Response.AddHeader("content-disposition", "attachment; filename=" + media.Name);
			Response.BinaryWrite(raw);
			Response.ContentType = "";
			Response.End();
		}
	}
}