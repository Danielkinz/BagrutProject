﻿delimiter //

CREATE DATABASE IF NOT EXISTS ShideaDB;//
USE ShideaDB;//




CREATE TABLE IF NOT EXISTS SectionTypes (
	_name VARCHAR(36) NOT NULL,
	PRIMARY KEY (_name)
);//

INSERT INTO SectionTypes(_name) VALUES ("Science"), ("Social"), ("Miscellaneous");//

CREATE TABLE IF NOT EXISTS Sections (
	_name VARCHAR(36) NOT NULL,
	_type VARCHAR(36) NOT NULL,
	PRIMARY KEY (_name),
	FOREIGN KEY (_type) REFERENCES SectionTypes(_name) ON DELETE CASCADE ON UPDATE NO ACTION
);//

INSERT INTO Sections(_name, _type) VALUES 
	("Physics", "Science"), ("Math", "Science"), ("Computer Science", "Science"), ("Electronics", "Science"),
	("History", "Social"), ("Education", "Social"), ("Law", "Social"),
	("Sport", "Miscellaneous"), ("Food", "Miscellaneous");//

CREATE TABLE IF NOT EXISTS Roles (
	_rolename VARCHAR(36),
	_rolestyle VARCHAR(512),
	PRIMARY KEY (_rolename)
);//

INSERT INTO Roles (_rolename, _rolestyle) VALUES ("Member", "color: gray; font-weight: bold;");//
INSERT INTO Roles (_rolename, _rolestyle) VALUES ("Administrator", "color: red; font-weight: bold");//

CREATE TABLE IF NOT EXISTS Media (
	_id INTEGER UNSIGNED AUTO_INCREMENT,
	_filename VARCHAR(128) NOT NULL DEFAULT "",
	_filesize BIGINT,
	_raw LONGBLOB,
	PRIMARY KEY (_id)
);//

INSERT INTO Media (_filename, _filesize, _raw) VALUES ("", 0, NULL);//

CREATE TABLE IF NOT EXISTS Users (
	_username VARCHAR(36) NOT NULL,
	_password VARCHAR(36) NOT NULL,
	_email VARCHAR(128) NOT NULL UNIQUE,
	_picture INTEGER UNSIGNED DEFAULT 1,
	_description VARCHAR(512) NOT NULL DEFAULT "",
	_role VARCHAR(36) DEFAULT "Member",
	_joinTime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
	PRIMARY KEY(_username),
	FOREIGN KEY (_picture) REFERENCES Media(_id),
	FOREIGN KEY (_role) REFERENCES Roles(_rolename)
);//

INSERT INTO Users (_username, _password, _email, _description, _role) VALUES ("Admin", "1234", "Contact@Shidea.com", "Yes, This is an admin. Now go search for someone else." , "Administrator");//

CREATE TABLE IF NOT EXISTS Content (
	_id INTEGER UNSIGNED AUTO_INCREMENT NOT NULL,
	_poster VARCHAR(36) NOT NULL,
	_content VARCHAR(2048) NOT NULL,
	_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
	PRIMARY KEY (_id),
	FOREIGN KEY (_poster) REFERENCES Users(_username) ON DELETE CASCADE ON UPDATE NO ACTION
);//

CREATE TABLE IF NOT EXISTS Ideas (
	_id INTEGER UNSIGNED AUTO_INCREMENT NOT NULL,
	_name VARCHAR(128) NOT NULL,
	_contentID INTEGER UNSIGNED NOT NULL,
	_section VARCHAR(36) NOT NULL, 
	_lastUpdate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
	PRIMARY KEY(_id),
	FOREIGN KEY (_section) REFERENCES Sections(_name) ON DELETE CASCADE ON UPDATE NO ACTION,
	FOREIGN KEY (_contentID) REFERENCES Content(_id) ON DELETE CASCADE ON UPDATE NO ACTION
);//

CREATE TABLE IF NOT EXISTS Comments (
	_id INTEGER UNSIGNED AUTO_INCREMENT NOT NULL,
	_idea INTEGER UNSIGNED NOT NULL,
	_contentID INTEGER UNSIGNED NOT NULL,
	PRIMARY KEY (_id),
	FOREIGN KEY (_idea) REFERENCES Ideas(_id) ON DELETE CASCADE ON UPDATE NO ACTION,
	FOREIGN KEY (_contentID) REFERENCES Content(_id) ON DELETE CASCADE ON UPDATE NO ACTION
);//

CREATE TABLE IF NOT EXISTS ContentMedia (
	_content INTEGER UNSIGNED NOT NULL,
	_media INTEGER UNSIGNED NOT NULL,
	FOREIGN KEY (_content) REFERENCES Content(_id) ON DELETE CASCADE ON UPDATE NO ACTION,
	FOREIGN KEY (_media) REFERENCES Media(_id) ON DELETE CASCADE ON UPDATE NO ACTION,
	PRIMARY KEY (_content, _media)
);//




DROP PROCEDURE IF EXISTS GetUser;//
CREATE PROCEDURE GetUser(IN username VARCHAR(36)) BEGIN
SELECT _username AS Username, _email AS Email, _description AS Description, _role AS Role, _joinTime AS JoinTime
FROM Users WHERE _username = username;
END;//

DROP PROCEDURE IF EXISTS AddUser;//
CREATE PROCEDURE AddUser(IN username VARCHAR(36), IN password VARCHAR(36), IN email VARCHAR(128)) BEGIN
START TRANSACTION;
INSERT INTO Users(_username, _password, _email) VALUES (username, password, email);
CALL GetUser(username);
COMMIT;
END;//

DROP PROCEDURE IF EXISTS CheckCredentials;//
CREATE PROCEDURE CheckCredentials(creds VARCHAR(128), password VARCHAR(36)) BEGIN
SELECT _username AS Username, _email AS Email, _description AS Description, _role AS Role, _joinTime AS JoinTime 
FROM Users WHERE (_username = creds OR _email = creds) AND _password = password;
END;//

DROP PROCEDURE IF EXISTS IsUsernameTaken;//
CREATE PROCEDURE IsUsernameTaken(username VARCHAR(36)) BEGIN
SELECT 1 FROM Users WHERE _username = username;
END;//

DROP PROCEDURE IF EXISTS IsEmailTaken;//
CREATE PROCEDURE IsEmailTaken(email VARCHAR(128)) BEGIN
SELECT 1 FROM Users WHERE _email = email;
END;//

DROP PROCEDURE IF EXISTS GetNewestMembers;//
CREATE PROCEDURE GetNewestMembers() BEGIN
SELECT _username AS Username, _email AS Email, _description AS Description, _role AS Role, _joinTime AS JoinTime
FROM Users ORDER BY JoinTime DESC LIMIT 5;
END;//

DROP PROCEDURE IF EXISTS UpdateUserDetails;//
CREATE PROCEDURE UpdateUserDetails(IN Username VARCHAR(36), IN Email VARCHAR(128), IN Description VARCHAR(512)) BEGIN
START TRANSACTION;
UPDATE Users SET _email = Email, _description = Description WHERE _username = Username;
COMMIT;
END;//

DROP PROCEDURE IF EXISTS UpdateUserPassword;//
CREATE PROCEDURE UpdateUserPassword(IN Username VARCHAR(36), IN Password VARCHAR(36)) BEGIN
START TRANSACTION;
UPDATE Users SET _password = Password WHERE _username = Username;
COMMIT;
END;//

DROP PROCEDURE IF EXISTS UpdateUserManagement;//
CREATE PROCEDURE UpdateUserManagement(IN Username VARCHAR(36), IN Role VARCHAR(36)) BEGIN
START TRANSACTION;
UPDATE Users SET _role = Role WHERE _username = Username;
COMMIT;
END;//

DROP PROCEDURE IF EXISTS UpdateUserPicture;//
CREATE PROCEDURE UpdateUserPicture(IN Username VARCHAR(36), IN PictureID INTEGER UNSIGNED) BEGIN
START TRANSACTION;
UPDATE Users SET _picture = PictureID WHERE _username = Username;
COMMIT;
END;//




DROP PROCEDURE IF EXISTS GetRoles;//
CREATE PROCEDURE GetRoles() BEGIN
SELECT _rolename FROM Roles;
END;//

DROP PROCEDURE IF EXISTS GetRoleStyle;//
CREATE PROCEDURE GetRoleStyle(rolename VARCHAR(36)) BEGIN
SELECT _rolestyle FROM Roles WHERE _rolename = rolename;
END;//

DROP PROCEDURE IF EXISTS AddRole;//
CREATE PROCEDURE AddRole(IN RoleName VARCHAR(36), IN RoleStyle VARCHAR(512)) BEGIN
START TRANSACTION;
INSERT INTO Roles(_rolename, _rolestyle) VALUES (RoleName, RoleStyle) ON DUPLICATE KEY UPDATE _rolename=_rolename;
COMMIT;
END;//

DROP PROCEDURE IF EXISTS UpdateRole;//
CREATE PROCEDURE UpdateRole(IN RoleName VARCHAR(36), IN RoleStyle VARCHAR(512)) BEGIN
START TRANSACTION;
UPDATE Roles SET _rolestyle=RoleStyle WHERE _rolename=RoleName;
COMMIT;
END;//

DROP PROCEDURE IF EXISTS DeleteRole;//
CREATE PROCEDURE DeleteRole(IN RoleName VARCHAR(36)) BEGIN
START TRANSACTION;
UPDATE Users SET _role="Member" WHERE _role=RoleName;
DELETE FROM Roles WHERE _rolename=RoleName;
COMMIT;
END;//



DROP PROCEDURE IF EXISTS GetMedia;//
CREATE PROCEDURE GetMedia(IN mediaID INTEGER UNSIGNED) BEGIN
SELECT _id AS ID, _filename AS FileName, _filesize AS FileSize FROM Media WHERE _id = mediaID;
END;//

DROP PROCEDURE IF EXISTS UploadMedia;//
CREATE PROCEDURE UploadMedia(IN filename VARCHAR(128), IN filesize BIGINT, IN Raw LONGBLOB) BEGIN
START TRANSACTION;
INSERT INTO Media (_filename, _filesize, _raw) VALUES (filename, filesize, Raw);
SELECT _id AS ID, _filename AS Filename, _filesize AS FileSize
FROM Media WHERE _id = LAST_INSERT_ID();
COMMIT;
END;//

DROP PROCEDURE IF EXISTS DownloadMedia;//
CREATE PROCEDURE DownloadMedia(IN mediaID INTEGER UNSIGNED) BEGIN
SELECT _fileSize AS FileSize, _raw AS Raw FROM Media WHERE _id = mediaID;
END;//

DROP PROCEDURE IF EXISTS GetProfilePicture;//
CREATE PROCEDURE GetProfilePicture(In User VARCHAR(36)) BEGIN
SELECT Media._id AS ID, Media._filename AS Filename, Media._filesize AS Filesize
FROM Users INNER JOIN Media ON Users._picture = Media._id
WHERE Users._username = User;
END;//

DROP PROCEDURE IF EXISTS LinkIdeaMedia;//
CREATE PROCEDURE LinkIdeaMedia(IN IdeaID INTEGER UNSIGNED, IN mediaID INTEGER UNSIGNED) BEGIN
START TRANSACTION;
INSERT INTO ContentMedia(_content, _media) SELECT _contentID, mediaID FROM Ideas WHERE _id=IdeaID ON DUPLICATE KEY UPDATE _media=_media;
COMMIT;
END;//

DROP PROCEDURE IF EXISTS LinkCommentMedia;//
CREATE PROCEDURE LinkCommentMedia(IN CommentID INTEGER UNSIGNED, IN mediaID INTEGER UNSIGNED) BEGIN
START TRANSACTION;
INSERT INTO ContentMedia(_content, _media) SELECT _contentID, mediaID FROM Comments WHERE _id=CommentID ON DUPLICATE KEY UPDATE _media=_media;
COMMIT;
END;//

DROP PROCEDURE IF EXISTS GetIdeaMediaList;//
CREATE PROCEDURE GetIdeaMediaList(IN IdeaID INTEGER UNSIGNED) BEGIN
SELECT Media._id AS ID, Media._filename AS FileName, Media._filesize AS FileSize
FROM Ideas 
	INNER JOIN ContentMedia ON Ideas._contentID = ContentMedia._content
	INNER JOIN Media ON ContentMedia._media = Media._id
WHERE Ideas._id = IdeaID;
END;//

DROP PROCEDURE IF EXISTS GetCommentMediaList;//
CREATE PROCEDURE GetCommentMediaList(IN CommentID INTEGER UNSIGNED) BEGIN
SELECT Media._id AS ID, Media._filename AS FileName, Media._filesize AS FileSize
FROM Comments 
	INNER JOIN ContentMedia ON Comments._contentID = ContentMedia._content
	INNER JOIN Media ON ContentMedia._media = Media._id
WHERE Comments._id = CommentID;
END;//




DROP PROCEDURE IF EXISTS GetIdea;//
CREATE PROCEDURE GetIdea(IN ideaID INTEGER UNSIGNED) BEGIN
SELECT Ideas._id AS ID, Ideas._name AS Name, Ideas._section AS Section, Content._poster AS Poster, Content._content AS Content, Content._time AS PostTime, Ideas._lastUpdate AS LastUpdate
FROM Ideas INNER JOIN Content ON Ideas._contentID = Content._id WHERE Ideas._id = ideaID;
END;//

DROP PROCEDURE IF EXISTS AddIdea;//
CREATE PROCEDURE AddIdea(IN name VARCHAR(128), IN description TEXT, IN poster VARCHAR(36), IN section VARCHAR(36)) BEGIN
START TRANSACTION;
INSERT INTO Content(_poster, _content) VALUES (poster, description);
INSERT INTO Ideas(_name, _contentID, _section) VALUES (name, LAST_INSERT_ID(), section);
CALL GetIdea(LAST_INSERT_ID());
COMMIT;
END;//

DROP PROCEDURE IF EXISTS GetIdeaContentID;//
CREATE PROCEDURE GetIdeaContentID(IN IdeaID INTEGER UNSIGNED) BEGIN
SELECT _contentID FROM Ideas WHERE _id = IdeaID;
END;//

DROP PROCEDURE IF EXISTS DeleteIdea;//
CREATE PROCEDURE DeleteIdea(IN IdeaID INTEGER UNSIGNED) BEGIN
START TRANSACTION;
DELETE FROM Ideas WHERE _id = IdeaID;
COMMIT;
END;//

DROP PROCEDURE IF EXISTS GetIdeasBySection;//
CREATE PROCEDURE GetIdeasBySection(IN section VARCHAR(36)) BEGIN
SELECT Ideas._id AS ID, Ideas._name AS Name, Ideas._section AS Section, Content._poster AS Poster, Content._content AS Content, Content._time AS PostTime, Ideas._lastUpdate AS LastUpdate
FROM Ideas INNER JOIN Content ON Ideas._contentID = Content._id WHERE Ideas._section = section ORDER BY LastUpdate DESC;
END;//

DROP PROCEDURE IF EXISTS GetIdeasBySectionLimited;//
CREATE PROCEDURE GetIdeasBySectionLimited(IN section VARCHAR(36), IN RecordsLimit INTEGER UNSIGNED, IN OffsetAmount INTEGER UNSIGNED) BEGIN
SELECT Ideas._id AS ID, Ideas._name AS Name, Ideas._section AS Section, Content._poster AS Poster, Content._content AS Content, Content._time AS PostTime, Ideas._lastUpdate AS LastUpdate 
FROM Ideas INNER JOIN Content ON Ideas._contentID = Content._id WHERE Ideas._section = section
ORDER BY LastUpdate DESC LIMIT RecordsLimit OFFSET OffsetAmount;
END;//

DROP PROCEDURE IF EXISTS GetLatestIdeas;//
CREATE PROCEDURE GetLatestIdeas() BEGIN
SELECT Ideas._id AS ID, Ideas._name AS Name, Ideas._section AS Section, Content._poster AS Poster, Content._content AS Content, Content._time AS PostTime, Ideas._lastUpdate AS LastUpdate
FROM Ideas INNER JOIN Content ON Ideas._contentID = Content._id ORDER BY LastUpdate DESC LIMIT 5;
END;//

DROP PROCEDURE IF EXISTS UpdateIdea;//
CREATE PROCEDURE UpdateIdea(IN ideaID INTEGER UNSIGNED, IN Name Varchar(36), IN Description VARCHAR(512)) BEGIN
START TRANSACTION;
UPDATE Ideas INNER JOIN Content ON Ideas._contentID = Content._id 
SET Ideas._name = Name, Content._content = Description 
WHERE Ideas._id = ideaID;
COMMIT;
END;//




DROP PROCEDURE IF EXISTS GetComment;//
CREATE PROCEDURE GetComment(IN commentID INTEGER UNSIGNED) BEGIN
SELECT Comments._id AS CommentID, Comments._idea AS Idea, Content._poster AS Poster, Content._content AS Content, Content._time AS Time
FROM Comments INNER JOIN Content ON Comments._contentID = Content._id WHERE Comments._id = commentID;
END;//

DROP PROCEDURE IF EXISTS AddComment;//
CREATE PROCEDURE AddComment(IN idea INTEGER UNSIGNED, IN description TEXT, IN poster VARCHAR(36)) BEGIN
START TRANSACTION;
UPDATE Ideas SET _lastUpdate=CURRENT_TIMESTAMP() WHERE _id = idea;
INSERT INTO Content(_poster, _content) VALUES (poster, description);
INSERT INTO Comments(_idea, _contentID) VALUES (idea, LAST_INSERT_ID());
CALL GetComment(LAST_INSERT_ID());
COMMIT;
END;//

DROP PROCEDURE IF EXISTS GetComments;//
CREATE PROCEDURE GetComments(IN ideaID INTEGER UNSIGNED) BEGIN
SELECT Comments._id AS CommentID, Content._poster AS Poster, Content._content AS Content, Content._time AS Time
FROM Comments INNER JOIN Content ON Comments._contentID = content._id 
WHERE Comments._idea = ideaID
ORDER BY Time;
END;//

DROP PROCEDURE IF EXISTS GetCommentContentID;//
CREATE PROCEDURE GetIdeaContentID(IN CommentID INTEGER UNSIGNED) BEGIN
SELECT _contentID FROM Comments WHERE _id = CommentID;
END;//

DROP PROCEDURE IF EXISTS DeleteComment;//
CREATE PROCEDURE DeleteComment(IN CommentID INTEGER UNSIGNED) BEGIN
START TRANSACTION;
DELETE FROM Comments WHERE _id = CommentID;
COMMIT;
END;//

DROP PROCEDURE IF EXISTS UpdateComment;//
CREATE PROCEDURE UpdateComment(IN CommentID INTEGER UNSIGNED, IN Description VARCHAR(512)) BEGIN
START TRANSACTION;
UPDATE Comments INNER JOIN Content ON Comments._contentID = Content._id 
SET Content._content = Description 
WHERE Comments._id = CommentID;
COMMIT;
END;//




DROP PROCEDURE IF EXISTS CreateSectionType;//
CREATE PROCEDURE CreateSectionType(IN TypeName VARCHAR(36)) BEGIN
START TRANSACTION;
INSERT INTO SectionTypes(_name) VALUES (TypeName) ON DUPLICATE KEY UPDATE _name=_name;
COMMIT;
END;//

DROP PROCEDURE IF EXISTS DeleteSectionType;//
CREATE PROCEDURE DeleteSectionType(IN TypeName VARCHAR(36)) BEGIN
START TRANSACTION;
DELETE FROM SectionTypes WHERE _name = TypeName;
COMMIT;
END;//

DROP PROCEDURE IF EXISTS GetSectionTypes;//
CREATE PROCEDURE GetSectionTypes() BEGIN
SELECT _name AS Name FROM SectionTypes; 
END;//




DROP PROCEDURE IF EXISTS CreateSection;//
CREATE PROCEDURE CreateSection(IN SectionName VARCHAR(36), SectionType VARCHAR(36)) BEGIN
START TRANSACTION;
INSERT INTO Sections(_name, _type) VALUES (SectionName, SectionType) ON DUPLICATE KEY UPDATE _name=_name;
COMMIT;
END;//

DROP PROCEDURE IF EXISTS DeleteSection;//
CREATE PROCEDURE DeleteSection(IN SectionName VARCHAR(36)) BEGIN
START TRANSACTION;
DELETE FROM Sections WHERE _name = SectionName;
COMMIT;
END;//

DROP PROCEDURE IF EXISTS GetSections;//
CREATE PROCEDURE GetSections(IN SectionType VARCHAR(36)) BEGIN
SELECT _name AS Name FROM Sections WHERE _type = SectionType; 
END;//



delimiter ;
