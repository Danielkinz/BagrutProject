﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

/// <summary>
/// Summary description for Comment
/// </summary>
[DataContract]
public class Comment {
	private uint commentID = 0;
	private uint ideaID = 0;
	private string description = null;
	private User poster = null;
	private DateTime postTime = DateTime.Now;

	[DataMember]
	public uint CommentID { get { return commentID; } set { commentID = value; } }

	[DataMember]
	public uint IdeaID { get { return ideaID; } set { ideaID = value; } }

	[DataMember]
	public string Description { get { return description; } set { description = value; } }

	[DataMember]
	public User Poster { get { return poster; } set { poster = value; } }

	[DataMember]
	public DateTime PostTime { get { return postTime; } set { postTime = value; } }

	public Comment(uint comment, uint idea, string description, User poster, DateTime postTime) {
		this.commentID = comment;
		this.ideaID = idea;
		this.description = description;
		this.poster = poster;
		this.postTime = postTime;
	}

	[OperationContract]
	public List<Media> GetAttachments() {
		return null;
	}
}