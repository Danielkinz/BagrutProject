﻿using System.Runtime.Serialization;
using System.ServiceModel;

/// <summary>
/// Summary description for Media
/// </summary>
[DataContract]
public class Media {
	private uint id = 0;
	private string name = null;
	private uint filesize = 0;

	[DataMember]
	public uint ID { get { return id; } set { id = value; } }

	[DataMember]
	public string Name { get { return name; } set { name = value; } }

	[DataMember]
	public uint FileSize { get { return filesize; } set { filesize = value; } }

	public Media(uint id, string name, uint filesize) {
		this.id = id;
		this.name = name == null ? "" : name;
		this.filesize = filesize;
	}
}