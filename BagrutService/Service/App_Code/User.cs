﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

/// <summary>
/// Summary description for User
/// </summary>
[DataContract]
public class User {
	private string username = null;
	private string email = null;
	private string description = null;
	private string role = null;
	private DateTime joinTime = DateTime.Now;

	[DataMember]
	public string Username { get { return username; } set { username = value; } }

	[DataMember]
	public string Email { get { return email; } set { email = value; } }

	[DataMember]
	public string Description { get { return description; } set { description = value; } }

	[DataMember]
	public string Role { get { return role; } set { role = value; } }

	[DataMember]
	public DateTime JoinTime { get { return joinTime; } set { joinTime = value; } }

	public User(string username, string email, string description, string role, DateTime joinTime) {
		this.username = username;
		this.email = email;
		this.description = description;
		this.role = role;
		this.joinTime = joinTime;
	}

	[OperationContract]
	public List<Media> GetAttachments() {
		return null;
	}
}