﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

/// <summary>
/// Summary description for Database
/// </summary>
public class Database {
	private const string ConnectionString = "server=localhost;uid=Daniel;pwd=1234;database=ShideaDB;SslMode=none;";

	public class Users {
		public static User GetUser(string username) {
			User ret = null;

			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL GetUser(@user);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@user", username);

			// Executes the command
			MySqlDataReader reader = cmd.ExecuteReader();
			if (reader.Read()) {
				ret = new User(reader.GetString("Username"),
								reader.GetString("Email"),
								reader.GetString("Description"),
								reader.GetString("Role"),
								reader.GetDateTime("JoinTime"));
			}
			reader.Close();
			DBConnection.Close();
			return ret;
		}

		public static User CheckCredentials(string credential, string password) {
			User ret = null;


			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL CheckCredentials(@credential, @password);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@credential", credential);
			cmd.Parameters.AddWithValue("@password", password);

			// Executes the command
			MySqlDataReader reader = cmd.ExecuteReader();
			if (reader.Read()) {
				ret = new User(reader.GetString("Username"),
								reader.GetString("Email"),
								reader.GetString("Description"),
								reader.GetString("Role"),
								reader.GetDateTime("JoinTime"));
			}
			reader.Close();
			DBConnection.Close();

			return ret;
		}

		public static User AddUser(string username, string password, string email) {
			User ret = null;

			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL AddUser(@username, @password, @email);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@username", username);
			cmd.Parameters.AddWithValue("@password", password);
			cmd.Parameters.AddWithValue("@email", email);

			// Executes the command
			MySqlDataReader reader = cmd.ExecuteReader();
			if (reader.Read()) {
				ret = new User(reader.GetString("Username"),
								reader.GetString("Email"),
								reader.GetString("Description"),
								reader.GetString("Role"),
								reader.GetDateTime("JoinTime"));
			}
			reader.Close();
			DBConnection.Close();
			return ret;
		}

		public static bool IsUsernameTaken(string username) {
			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL IsUsernameTaken(@username);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@username", username);

			// Executes the command
			MySqlDataReader reader = cmd.ExecuteReader();
			if (reader.Read()) {
				reader.Close();
				return true;
			}
			reader.Close();
			DBConnection.Close();
			return false;
		}

		public static bool IsEmailTaken(string username) {
			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL IsEmailTaken(@username);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@username", username);

			// Executes the command
			MySqlDataReader reader = cmd.ExecuteReader();
			if (reader.Read()) {
				reader.Close();
				return true;
			}
			reader.Close();
			DBConnection.Close();

			return false;
		}

		public static List<User> GetNewestUsers() {
			List<User> ideas = new List<User>();

			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL GetNewestMembers();", DBConnection);
			MySqlDataReader reader = cmd.ExecuteReader();

			while (reader.Read())
				ideas.Add(new User(reader.GetString("Username"),
									reader.GetString("Email"),
									reader.GetString("Description"),
									reader.GetString("Role"),
									reader.GetDateTime("JoinTime")));
			reader.Close();
			DBConnection.Close();
			return ideas;
		}

		public static void UpdateUserDetails(string username, string email, string description) {
			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL UpdateUserDetails(@username, @email, @description);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@username", username);
			cmd.Parameters.AddWithValue("@email", email);
			cmd.Parameters.AddWithValue("@description", description);

			// Executes the command
			cmd.ExecuteNonQuery();
			DBConnection.Close();
		}

		public static void UpdateUserPassword(string username, string password) {
			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL UpdateUserPassword(@username, @password);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@username", username);
			cmd.Parameters.AddWithValue("@password", password);

			// Executes the command
			cmd.ExecuteNonQuery();
			DBConnection.Close();
		}

		public static void UpdateUserManagement(string username, string role) {
			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL UpdateUserManagement(@username, @role);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@username", username);
			cmd.Parameters.AddWithValue("@role", role);

			// Executes the command
			cmd.ExecuteNonQuery();
			DBConnection.Close();
		}

		public static void SetProfilePicture(string username, uint pictureid) {
			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL UpdateUserPicture(@username, @pictureid);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@username", username);
			cmd.Parameters.AddWithValue("@pictureid", pictureid);

			// Executes the command
			cmd.ExecuteNonQuery();
			DBConnection.Close();
		}

		public static Media GetProfilePicture(string username) {
			Media ret = null;

			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL GetProfilePicture(@user);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@user", username);

			// Executes the command
			MySqlDataReader reader = cmd.ExecuteReader();
			if (reader.Read()) {
				ret = new Media(reader.GetUInt32("ID"), reader.GetString("Filename"), reader.GetUInt32("Filesize"));
			}
			reader.Close();
			DBConnection.Close();

			return ret;
		}
	}

	public class Role {
		public static List<string> GetRoles() {
			List<string> roles = new List<string>();

			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL GetRoles();", DBConnection);
			MySqlDataReader reader = cmd.ExecuteReader();

			while (reader.Read())
				roles.Add(reader.GetString("_rolename"));
			reader.Close();
			DBConnection.Close();

			return roles;
		}

		public static string GetRoleStyle(string role) {
			string ret = null;

			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL GetRoleStyle(@rolename);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@rolename", role);

			// Executes the command
			MySqlDataReader reader = cmd.ExecuteReader();
			if (reader.Read()) {
				ret = reader.GetString("_rolestyle");
			}
			reader.Close();
			DBConnection.Close();

			return ret;
		}

		public static void AddRole(string name, string style) {
			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL AddRole(@name, @style);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@name", name);
			cmd.Parameters.AddWithValue("@style", style);

			// Executes the command
			cmd.ExecuteNonQuery();
			DBConnection.Close();
		}

		public static void UpdateRole(string name, string style) {
			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL UpdateRole(@name, @style);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@name", name);
			cmd.Parameters.AddWithValue("@style", style);

			// Executes the command
			cmd.ExecuteNonQuery();
			DBConnection.Close();
		}

		public static void DeleteRole(string name) {
			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL DeleteRole(@name);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@name", name);

			// Executes the command
			cmd.ExecuteNonQuery();
			DBConnection.Close();
		}
	}

	public class MediaT {

		public static Media GetMedia(uint id) {
			Media ret = null;

			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL GetMedia(@id);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@id", id);

			// Executes the command
			MySqlDataReader reader = cmd.ExecuteReader();
			if (reader.Read()) {
				ret = new Media(reader.GetUInt32("ID"),
								reader.GetString("FileName"),
								reader.GetUInt32("FileSize"));
			}
			reader.Close();
			DBConnection.Close();

			return ret;
		}

		public static Media Upload(string filename, byte[] data) {
			Media ret = null;

			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL UploadMedia(@filename, @filesize, @raw);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@filename", filename);
			cmd.Parameters.AddWithValue("@filesize", data.Length);
			cmd.Parameters.AddWithValue("@raw", data);

			// Executes the command
			MySqlDataReader reader = cmd.ExecuteReader();
			if (reader.Read()) {
				ret = new Media(reader.GetUInt32("ID"), reader.GetString("Filename"), reader.GetUInt32("Filesize"));
			}
			reader.Close();
			DBConnection.Close();

			return ret;
		}

		public static byte[] Download(uint id) {
			byte[] rawData = null;

			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL DownloadMedia(@id);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@id", id);

			MySqlDataReader reader = cmd.ExecuteReader();

			if (reader.Read()) {
				rawData = new byte[reader.GetInt32("FileSize")];
				reader.GetBytes(reader.GetOrdinal("Raw"), 0, rawData, 0, rawData.Length);
			}

			reader.Close();
			DBConnection.Close();

			if (rawData == null) rawData = new byte[0];
			return rawData;
		}
	}

	public class Ideas {
		public static Idea AddIdea(string name, string description, string poster, string section) {
			Idea ret = null;

			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL AddIdea(@name, @description, @poster, @section);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@name", name);
			cmd.Parameters.AddWithValue("@description", description);
			cmd.Parameters.AddWithValue("@poster", poster);
			cmd.Parameters.AddWithValue("@section", section);

			// Executes the command
			MySqlDataReader reader = cmd.ExecuteReader();
			if (reader.Read()) {
				ret = new Idea(reader.GetUInt32("ID"),
								reader.GetString("Name"),
								reader.GetString("Content"),
								Users.GetUser(reader.GetString("Poster")),
								reader.GetString("Section"),
								reader.GetDateTime("PostTime"),
								reader.GetDateTime("LastUpdate"));
			}
			reader.Close();
			DBConnection.Close();

			return ret;
		}

		public static Idea GetIdea(uint ideaID) {
			Idea ret = null;

			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL GetIdea(@id);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@id", ideaID);

			// Executes the command
			MySqlDataReader reader = cmd.ExecuteReader();
			if (reader.Read()) {
				ret = new Idea(reader.GetUInt32("ID"),
								reader.GetString("Name"),
								reader.GetString("Content"),
								Users.GetUser(reader.GetString("Poster")),
								reader.GetString("Section"),
								reader.GetDateTime("PostTime"),
								reader.GetDateTime("LastUpdate"));
			}
			reader.Close();
			DBConnection.Close();

			return ret;
		}

		public static void DeleteIdea(uint ideaID) {
			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL DeleteIdea(@idea);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@idea", ideaID);

			// Executes the command
			cmd.ExecuteNonQuery();
			DBConnection.Close();
		}

		public static List<Idea> GetIdeasBySection(string section) {
			List<Idea> ideas = new List<Idea>();

			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL GetIdeasBySection(@section);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@section", section);
			MySqlDataReader reader = cmd.ExecuteReader();

			while (reader.Read())
				ideas.Add(new Idea(reader.GetUInt32("ID"),
									reader.GetString("Name"),
									reader.GetString("Content"),
									Users.GetUser(reader.GetString("Poster")),
									reader.GetString("Section"),
									reader.GetDateTime("PostTime"),
									reader.GetDateTime("LastUpdate")));
			reader.Close();
			DBConnection.Close();

			return ideas;
		}

		public static List<Idea> GetIdeasBySectionLimited(string section, uint limit, uint offset) {
			List<Idea> ideas = new List<Idea>();

			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL GetIdeasBySectionLimited(@section, @limit, @offset);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@section", section);
			cmd.Parameters.AddWithValue("@limit", limit);
			cmd.Parameters.AddWithValue("@offset", offset);
			MySqlDataReader reader = cmd.ExecuteReader();

			while (reader.Read())
				ideas.Add(new Idea(reader.GetUInt32("ID"),
									reader.GetString("Name"),
									reader.GetString("Content"),
									Users.GetUser(reader.GetString("Poster")),
									reader.GetString("Section"),
									reader.GetDateTime("PostTime"),
									reader.GetDateTime("LastUpdate")));
			reader.Close();
			DBConnection.Close();

			return ideas;
		}

		public static List<Idea> GetLatestIdeas() {
			List<Idea> ideas = new List<Idea>();

			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL GetLatestIdeas();", DBConnection);
			MySqlDataReader reader = cmd.ExecuteReader();

			while (reader.Read()) {
				ideas.Add(new Idea(reader.GetUInt32("ID"),
									reader.GetString("Name"),
									reader.GetString("Content"),
									Users.GetUser(reader.GetString("Poster")),
									reader.GetString("Section"),
									reader.GetDateTime("PostTime"),
									reader.GetDateTime("LastUpdate")));
			}

			reader.Close();
			DBConnection.Close();

			return ideas;
		}

		public static void UpdateIdea(uint idea, string name, string description) {
			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL UpdateIdea(@idea, @name, @description);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@idea", idea);
			cmd.Parameters.AddWithValue("@name", name);
			cmd.Parameters.AddWithValue("@description", description);

			// Executes the command
			cmd.ExecuteNonQuery();
			DBConnection.Close();
		}

		public static void LinkMedia(uint idea, uint media) {
			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL LinkIdeaMedia(@idea, @media);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@idea", idea);
			cmd.Parameters.AddWithValue("@media", media);

			// Executes the command
			cmd.ExecuteNonQuery();
			DBConnection.Close();
		}

		public static List<Media> GetMediaList(uint idea) {
			List<Media> mediaList = new List<Media>();

			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL GetIdeaMediaList(@idea);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@idea", idea);
			MySqlDataReader reader = cmd.ExecuteReader();

			while (reader.Read()) {
				mediaList.Add(new Media(reader.GetUInt32("ID"),
								reader.GetString("FileName"),
								reader.GetUInt32("FileSize")));
			}

			reader.Close();
			DBConnection.Close();

			return mediaList;
		}
	}

	public class Comments {
		public static Comment AddComment(uint idea, string description, string poster) {
			Comment ret = null;

			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL AddComment(@idea, @description, @poster);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@idea", idea);
			cmd.Parameters.AddWithValue("@description", description);
			cmd.Parameters.AddWithValue("@poster", poster);

			// Executes the command
			MySqlDataReader reader = cmd.ExecuteReader();
			if (reader.Read()) {
				ret = new Comment(reader.GetUInt32("CommentID"),
									reader.GetUInt32("Idea"),
									reader.GetString("Content"),
									Users.GetUser(reader.GetString("Poster")),
									reader.GetDateTime("Time"));
			}
			reader.Close();
			DBConnection.Close();

			return ret;
		}

		public static Comment GetComment(uint commentID) {
			Comment ret = null;

			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL GetComment(@id);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@id", commentID);

			// Executes the command
			MySqlDataReader reader = cmd.ExecuteReader();
			if (reader.Read()) {
				ret = new Comment(reader.GetUInt32("CommentID"),
									reader.GetUInt32("Idea"),
									reader.GetString("Content"),
									Users.GetUser(reader.GetString("Poster")),
									reader.GetDateTime("Time"));
			}
			reader.Close();
			DBConnection.Close();

			return ret;
		}

		public static List<Comment> GetComments(uint ideaID) {
			List<Comment> comments = new List<Comment>();

			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL GetComments(@idea)", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@idea", ideaID);
			MySqlDataReader reader = cmd.ExecuteReader();

			while (reader.Read()) {
				comments.Add(new Comment(reader.GetUInt32("CommentID"),
											ideaID,
											reader.GetString("Content"),
											Users.GetUser(reader.GetString("Poster")),
											reader.GetDateTime("Time")));
			}
			reader.Close();
			DBConnection.Close();

			return comments;
		}

		public static void DeleteComment(uint commentID) {
			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL DeleteComment(@comment);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@comment", commentID);

			// Executes the command
			cmd.ExecuteNonQuery();
			DBConnection.Close();
		}

		public static void UpdateComment(uint commentID, string description) {
			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL UpdateComment(@comment, @description);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@comment", commentID);
			cmd.Parameters.AddWithValue("@description", description);

			// Executes the command
			cmd.ExecuteNonQuery();
			DBConnection.Close();
		}

		public static void LinkMedia(uint comment, uint media) {
			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL LinkCommentMedia(@comment, @media);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@comment", comment);
			cmd.Parameters.AddWithValue("@media", media);

			// Executes the command
			cmd.ExecuteNonQuery();
			DBConnection.Close();
		}

		public static List<Media> GetMediaList(uint comment) {
			List<Media> mediaList = new List<Media>();

			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL GetCommentMediaList(@comment);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@comment", comment);
			MySqlDataReader reader = cmd.ExecuteReader();

			while (reader.Read()) {
				mediaList.Add(new Media(reader.GetUInt32("ID"),
								reader.GetString("FileName"),
								reader.GetUInt32("FileSize")));
			}

			reader.Close();
			DBConnection.Close();

			return mediaList;
		}
	}
	
	public class Sections {
		public static void CreateSectionType(string name) {
			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL CreateSectionType(@name);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@name", name);

			// Executes the command
			cmd.ExecuteNonQuery();
			DBConnection.Close();
		}

		public static void DeleteSectionType(string name) {
			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL DeleteSectionType(@name);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@name", name);

			// Executes the command
			cmd.ExecuteNonQuery();
			DBConnection.Close();
		}

		public static List<string> GetSectionTypes() {
			List<string> tags = new List<string>();

			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL GetSectionTypes();", DBConnection);
			MySqlDataReader reader = cmd.ExecuteReader();

			while (reader.Read())
				tags.Add(reader.GetString("Name"));
			reader.Close();
			DBConnection.Close();

			return tags;
		}

		public static void CreateSection(string name, string type) {
			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL CreateSection(@name, @type);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@name", name);
			cmd.Parameters.AddWithValue("@type", type);

			// Executes the command
			cmd.ExecuteNonQuery();
			DBConnection.Close();
		}

		public static void DeleteSection(string name) {
			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL DeleteSection(@name);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@name", name);

			// Executes the command
			cmd.ExecuteNonQuery();
			DBConnection.Close();
		}

		public static List<string> GetSections(string type) {
			List<string> tags = new List<string>();

			MySqlConnection DBConnection = new MySqlConnection(ConnectionString); DBConnection.Open();

			// Creates the command
			MySqlCommand cmd = new MySqlCommand("CALL GetSections(@type);", DBConnection);
			cmd.Prepare();
			cmd.Parameters.AddWithValue("@type", type);
			MySqlDataReader reader = cmd.ExecuteReader();

			while (reader.Read())
				tags.Add(reader.GetString("Name"));
			reader.Close();
			DBConnection.Close();
			return tags;
		}
	}
}
