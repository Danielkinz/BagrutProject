﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

/// <summary>
/// Summary description for Idea
/// </summary>
[DataContract]
public class Idea {
	private uint id = 0;
	private string name = null;
	private string description = null;
	private User poster = null;
	private string section = null;
	private DateTime postTime = DateTime.Now;
	private DateTime lastUpdate = DateTime.Now;

	[DataMember]
	public uint ID { get { return id; } set { id = value; } }

	[DataMember]
	public string Name { get { return name; } set { name = value; } }

	[DataMember]
	public string Description { get { return description; } set { description = value; } }

	[DataMember]
	public User Poster { get { return poster; } set { poster = value; } }

	[DataMember]
	public string Section { get { return section; } set { section = value; } }

	[DataMember]
	public DateTime PostTime { get { return postTime; } set { postTime = value; } }

	[DataMember]
	public DateTime LastUpdate { get { return lastUpdate; } set { lastUpdate = value; } }

	public Idea(uint id, string name, string description, User poster, string section, DateTime postTime, DateTime lastUpdate) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.poster = poster;
		this.section = section;
		this.postTime = postTime;
		this.lastUpdate = lastUpdate;
	}

	[OperationContract]
	public List<Media> GetAttachments() {
		return null;
	}
}