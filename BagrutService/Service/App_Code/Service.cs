﻿using System;
using System.Collections;
using System.Collections.Generic;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service" in code, svc and config file together.
public class Service : IService
{
	public Service() {}

	// ------------------- User ------------------- //

	public User CheckCredentials(string credential, string password) {
		return Database.Users.CheckCredentials(credential, password);
	}
	
	public User AddUser(string username, string password, string email) {
		return Database.Users.AddUser(username, password, email);
	}

	public User GetUser(string username) {
		return Database.Users.GetUser(username);
	}

	public bool IsUsernameTaken(string username) {
		return Database.Users.IsUsernameTaken(username);
	}

	public bool IsEmailTaken(string email) {
		return Database.Users.IsEmailTaken(email);
	}

	public List<User> GetNewsetUsers() {
		return Database.Users.GetNewestUsers();
	}

	public void UpdateUserDetails(string username, string email, string description) {
		Database.Users.UpdateUserDetails(username, email, description);
	}

	public void UpdateUserPassword(string username, string password) {
		Database.Users.UpdateUserPassword(username, password);
	}

	public void UpdateUserManagement(string username, string role) {
		Database.Users.UpdateUserManagement(username, role);
	}

	public Media GetProfilePicture(string username) {
		return Database.Users.GetProfilePicture(username);
	}

	public void SetProfilePicture(string username, uint pictureid) {
		Database.Users.SetProfilePicture(username, pictureid);
	}

	// ------------------- Role ------------------- //

	public List<string> GetRoles() {
		return Database.Role.GetRoles();
	}
	
	public string GetRoleStyle(string role) {
		return Database.Role.GetRoleStyle(role);
	}

	public void AddRole(string name, string style) {
		Database.Role.AddRole(name, style);
	}

	public void DeleteRole(string name) {
		Database.Role.DeleteRole(name);
	}

	public void UpdateRole(string name, string style) {
		Database.Role.UpdateRole(name, style);
	}

	// ------------------- Media ------------------- //

	private class MediaTransferSession {
		public string filename = "";
		public byte[] buffer;
		public uint transferred = 0;
		public uint filesize = 0;
		public MediaTransferSession(string filename, uint filesize) {
			this.filename = filename;
			this.filesize = filesize;
			buffer = new byte[filesize];
		} 
	}

	private static List<MediaTransferSession> MediaSessions = new List<MediaTransferSession>();

	public int StartMediaUpload(string filename, uint filesize) {
		MediaSessions.Add(new MediaTransferSession(filename, filesize));
		return MediaSessions.Count-1;
	}

	public void StreamMedia(int session, byte[] data) {
		data.CopyTo(MediaSessions[session].buffer, MediaSessions[session].transferred);
		MediaSessions[session].transferred += (uint) data.Length;
	}

	public Media EndMediaUpload(int session) {
		return Database.MediaT.Upload(MediaSessions[session].filename, MediaSessions[session].buffer);
	}

	public int StartMediaDownload(uint mediaID) {
		Media media = Database.MediaT.GetMedia(mediaID);
		MediaSessions.Add(new MediaTransferSession(media.Name, media.FileSize));
		MediaSessions[MediaSessions.Count - 1].buffer = Database.MediaT.Download(mediaID);
		MediaSessions[MediaSessions.Count - 1].filesize = (uint) MediaSessions[MediaSessions.Count - 1].buffer.Length;
		return MediaSessions.Count - 1;
	}

	public byte[] DownloadMedia(int session) {
		byte[] ret = null;
		MediaTransferSession sess = MediaSessions[session];
		
		if (sess.filesize - sess.transferred > 8192) {
			ret = new byte[8192];
			Array.Copy(sess.buffer, sess.transferred, ret, 0, 8192);
			sess.transferred += 8192;
		} else {
			ret = new byte[sess.filesize - sess.transferred];
			Array.Copy(sess.buffer, sess.transferred, ret, 0, sess.filesize - sess.transferred);
			sess.transferred = sess.filesize;
		}

		return ret;
	}

	public Media GetMedia(uint mediaID) {
		return Database.MediaT.GetMedia(mediaID);
	}

	// ------------------- Ideas ------------------- //

	public Idea AddIdea(string name, string description, string poster, string section) {
		return Database.Ideas.AddIdea(name, description, poster, section);
	}
	
	public Idea GetIdea(uint ideaID) {
		return Database.Ideas.GetIdea(ideaID);
	}

	public void DeleteIdea(uint ideaID) {
		Database.Ideas.DeleteIdea(ideaID);
	}

	public List<Idea> GetIdeasBySection(string section) {
		return Database.Ideas.GetIdeasBySection(section);
	}

	public List<Idea> GetIdeasBySectionPage(string section, uint limit, uint page) {
		return Database.Ideas.GetIdeasBySectionLimited(section, limit, (page-1) * limit);
	}

	public List<Idea> GetLatestIdeas() {
		return Database.Ideas.GetLatestIdeas();
	}

	public void UpdateIdea(uint ideaId, string name, string description) {
		Database.Ideas.UpdateIdea(ideaId, name, description);
	}

	public void LinkIdeaMedia(uint idea, uint media) {
		Database.Ideas.LinkMedia(idea, media);
	}

	public List<Media> GetIdeaMediaList(uint idea) {
		return Database.Ideas.GetMediaList(idea);
	}

	// ------------------- Comments ------------------- //

	public Comment AddComment(uint idea, string description, string poster) {
		return Database.Comments.AddComment(idea, description, poster);
	}

	public Comment GetComment(uint commentID) {
		return Database.Comments.GetComment(commentID);
	}

	public List<Comment> GetComments(uint ideaID) {
		return Database.Comments.GetComments(ideaID);
	}

	public void DeleteComment(uint comment) {
		Database.Comments.DeleteComment(comment);
	}

	public void UpdateComment(uint commentID, string description) {
		Database.Comments.UpdateComment(commentID, description);
	}

	public void LinkCommentMedia(uint comment, uint media) {
		Database.Comments.LinkMedia(comment, media);
	}

	public List<Media> GetCommentMediaList(uint comment) {
		return Database.Comments.GetMediaList(comment);
	}

	// ------------------- Sections ------------------- //

	public void CreateSectionType(string name) {
		Database.Sections.CreateSectionType(name);
	}

	public void DeleteSectionType(string name) {
		Database.Sections.DeleteSectionType(name);
	}

	public List<string> GetSectionTypes() {
		return Database.Sections.GetSectionTypes();
	}

	public void CreateSection(string name, string type) {
		Database.Sections.CreateSection(name, type);
	}

	public void DeleteSection(string name) {
		Database.Sections.DeleteSection(name);
	}

	public List<string> GetSections(string type) {
		return Database.Sections.GetSections(type);
	}
}
