﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService" in both code and config file together.
[ServiceContract]
public interface IService {

	// ------------------- User ------------------- //

	[OperationContract]
	User CheckCredentials(string credential, string password);

	[OperationContract]
	User AddUser(string username, string password, string email);

	[OperationContract]
	User GetUser(string username);

	[OperationContract]
	bool IsUsernameTaken(string username);

	[OperationContract]
	bool IsEmailTaken(string email);

	[OperationContract]
	List<User> GetNewsetUsers();

	[OperationContract]
	void UpdateUserDetails(string username, string email, string description);

	[OperationContract]
	void UpdateUserPassword(string username, string password);

	[OperationContract]
	void UpdateUserManagement(string username, string role);

	[OperationContract]
	Media GetProfilePicture(string username);

	[OperationContract]
	void SetProfilePicture(string username, uint pictureid);

	// ------------------- Role ------------------- //

	[OperationContract]
	List<string> GetRoles();

	[OperationContract]
	string GetRoleStyle(string role);

	[OperationContract]
	void AddRole(string name, string style);

	[OperationContract]
	void DeleteRole(string name);

	[OperationContract]
	void UpdateRole(string name, string style);

	// ------------------- Media ------------------- //

	[OperationContract]
	int StartMediaUpload(string filename, uint filesize);

	[OperationContract]
	void StreamMedia(int session, byte[] data);

	[OperationContract]
	Media EndMediaUpload(int session);

	[OperationContract]
	int StartMediaDownload(uint mediaID);

	[OperationContract]
	byte[] DownloadMedia(int session);

	[OperationContract]
	Media GetMedia(uint mediaID);

	// ------------------- Ideas ------------------- //

	[OperationContract]
	Idea AddIdea(string name, string description, string poster, string section);

	[OperationContract]
	Idea GetIdea(uint ideaID);

	[OperationContract]
	void DeleteIdea(uint ideaID);

	[OperationContract]
	List<Idea> GetIdeasBySection(string section);

	[OperationContract]
	List<Idea> GetIdeasBySectionPage(string section, uint limit, uint page);

	[OperationContract]
	List<Idea> GetLatestIdeas();

	[OperationContract]
	void UpdateIdea(uint ideaId, string name, string description);

	[OperationContract]
	void LinkIdeaMedia(uint idea, uint media);

	[OperationContract]
	List<Media> GetIdeaMediaList(uint idea);

	// ------------------- Comments ------------------- //

	[OperationContract]
	Comment AddComment(uint idea, string description, string poster);

	[OperationContract]
	Comment GetComment(uint commentID);

	[OperationContract]
	List<Comment> GetComments(uint ideaID);

	[OperationContract]
	void DeleteComment(uint comment);

	[OperationContract]
	void UpdateComment(uint ideaID, string description);

	[OperationContract]
	void LinkCommentMedia(uint comment, uint media);

	[OperationContract]
	List<Media> GetCommentMediaList(uint comment);

	// ------------------- Sections ------------------- //

	[OperationContract]
	void CreateSectionType(string name);

	[OperationContract]
	void DeleteSectionType(string name);

	[OperationContract]
	List<string> GetSectionTypes();

	[OperationContract]
	void CreateSection(string name, string type);

	[OperationContract]
	void DeleteSection(string name);

	[OperationContract]
	List<string> GetSections(string type);
}
